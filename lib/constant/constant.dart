import 'package:flutter_screenutil/flutter_screenutil.dart';

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';

const SUCCESS = '200';
const TIME_OUT = '498.2';
const ERROR_INPUT = '408.2';
const ERROR_INPUT_SEARCH = '203';

const CONFIG_BASE_URL = 'https://qlts.vnerp.vn/api/v1';
const CONFIG_BASE_URL1 = 'http://10.1.123.7:8088/api/v1';

const HEIGHT_INVENTORY_QRCODE_NO_DATA = 660.0;
const HEIGHT_INVENTORY_QRCODE_DATA = 1010.0;

double ws = ScreenUtil().setWidth(3.5);
double hs = ScreenUtil().setHeight(1.75);
double fs = ScreenUtil().setSp(4);

const double CUPERTINO_TAB_BAR_HEIGHT = 50;

///
/// ----------------Url webview-------------
///
const URL ='https://id.vnpt.com.vn/cas/login?service=https://qlts.vnerp.vn/web/loginapp?response_type=token&client_id=False&redirect_uri=http%3A%2F%2Fqlts.vnerp.vn%2Fauth_oauth%2Fsignin&scope=False&state=%7B%22d%22%3A+%22vnpt_qlts%22%2C+%22p%22%3A+4%2C+%22r%22%3A+%22http%253A%252F%252Fqlts.vnerp.vn%252Fweb%22%7D';
const URL_LOGOUT = 'https://id.vnpt.com.vn/cas/logout?service=https://id.vnpt.com.vn/cas/login?service=https://qlts.vnerp.vn/web/loginapp?response_type=token&client_id=False&redirect_uri=http%3A%2F%2Fqlts.vnerp.vn%2Fauth_oauth%2Fsignin&scope=False&state=%7B%22d%22%3A+%22vnpt_qlts%22%2C+%22p%22%3A+4%2C+%22r%22%3A+%22http%253A%252F%252Fqlts.vnerp.vn%252Fweb%22%7D';

///
///------------Error Notification----------------
///
const ERROR_NOT_FOUND = 'Không tìm thấy tài sản tương ứng';
const EMPTY_SQFLITE = 'Không có dữ liệu kiểm kê offline';
const EMPTY_UPDATED = 'Chưa có tài sản nào được kiểm kê';
const EMPTY_IMAGE ='Chưa có ảnh hiện trường';
const WRONG_QRCODE = 'Dữ liệu nhập vào không đúng!';
const LACK_OF_INVENTORY = 'Không có đợt kiểm kê tài sản';
const LACK_OF_FIXED_INVENTORY = 'Thiếu danh sách chốt sổ';

const LACK_OF_INFORMATION_INPUT = '*Vui lòng nhập thông tin kiểm kê';
const LACK_OF_INFORMATION_UPDATE = 'Không có dữ liệu';

const ERROR_DOWLOAD = 'Tải dữ liệu thất bại!';
const SUCCESS_DOWLOAD = 'Thành công tải dữ liệu!';
const SUCCESS_UPLOAD = 'Thành công upload dữ liệu!';
const SUCCESS_UPDATE = 'Thành công cập nhật dữ liệu!';
const FAIL_UPDATE = 'Cập nhật thất bại!';

const FAIL_INVENTORY = 'Vui lòng thực hiện lại';

///
/// -------------Tabbar title
///
const TABBAR_TITLE_QRCODE = 'QR Code';
const TABBAR_TITLE_INPUT = 'Tra cứu thông tin';
const TABBAR_TITLE_INVENTORY_HOME = 'Đợt kiểm kê';

///
/// ------Home Page-------
///
const TITLE_SEARCH = 'Tra cứu tài sản';
const TITLE_INVENTORY = 'Kiểm kê tài sản';

///
/// ---------Title-------
///

const CHON_DOT = 'Chọn đợt (*)';
const CHON_SO = 'Chọn sổ';
const KET_NOI = 'Kết nối (*)';
const KET_NOI_TITLE = 'Kết nối';
const DANH_SACH_CHOT_SO = 'Danh sách chốt sổ';
const ALLOW_CAMERA = 'Truy cập camera';

const SO_THE = 'Số thẻ';
const ID_CHI_TIET = 'Và Id chi tiết';
const SERIAL = 'Hoặc Serial';
const QRCODE = 'Hoặc QR code';

const THONG_TIN_TAI_SAN = 'Thông tin tài sản';

const TINH_TRANG_KIEM_KE = 'Tình trạng kiểm kê';
const EMPTY_TTKK = 'Không có tình trạng kiểm kê tài sản';

const EMPTY_FIXED_INVENTORY = 'Không có danh sách chốt sổ';
const LIST_INVENTORY = 'Các đợt kiểm kê tài sản';

///
/// ---------Hint Text----------
///
const HINT_SO_THE = 'Nhập số thẻ';
const HINT_ID_CHI_TIET = 'Nhập mã số';
const HINT_SERIAL = 'Nhập serial number';
const HINT_QRCODE = 'Nhập Qrcode';

///
/// --------Title button
///
const CHUP_ANH = 'Chụp ảnh hiện trường';
const TRA_CUU = 'Tra cứu';
const DOWLOAD = 'Tải dữ liệu offline';
const UPLOAD = 'Upload dữ liệu';
const CHON = 'Chọn';
const ANH_HIEN_TRUONG = 'Xem ảnh hiện trường';
const CAP_NHAT = 'Cập nhật';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_result_API_model.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_home/my_home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashLoginPage extends StatefulWidget {
  final data;

  SplashLoginPage({this.data});

  @override
  State<StatefulWidget> createState() => SplashLoginPageState();
}

class SplashLoginPageState extends State<SplashLoginPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Repository repository = Repository();

  @override
  void initState() {
    super.initState();

    saveToken();
  }

  void saveToken() async {
    print('-------------save token');
    if (widget.data != null) {
      print('-------------widget.data != null');
      String token1 = widget.data.substring(
          widget.data.lastIndexOf('"token": "') + 10, widget.data.length - 2);
      String email = widget.data.substring(
          widget.data.lastIndexOf('"email": "') + 10,
          widget.data.lastIndexOf('"token": "') - 3);

      print('----------$token1\n$email\n-------------');
      InventorLoginyResultAPIModel inventorLoginyResultAPIModel =
          await repository.loginAPIProvider.fetchLogin(token1, email, 'abc');

      if (inventorLoginyResultAPIModel.errorCode != '200') {
        print(
            '${inventorLoginyResultAPIModel.errorCode} :${inventorLoginyResultAPIModel.errorDesc}');

        showErrorDialog(
            context: context,
            errorCode: 'login',
            errorDesc: inventorLoginyResultAPIModel.errorDesc);
      } else {
        if (inventorLoginyResultAPIModel.token == '' ||
            inventorLoginyResultAPIModel.token == null) {
          showErrorDialog(
              context: context,
              errorCode: 'login',
              errorDesc: inventorLoginyResultAPIModel.errorDesc);
        } else {
          print(
              '${inventorLoginyResultAPIModel.errorCode} :${inventorLoginyResultAPIModel.errorDesc}');
          print(
              '******MyHomePage**token******\n${inventorLoginyResultAPIModel.token}\n******************');
          print('************************************');
          SharedPreferences prefs = await _prefs;
          prefs.setString('token', inventorLoginyResultAPIModel.token);

          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      MyHomePage(token: inventorLoginyResultAPIModel.token)));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        // child: CircularProgressIndicator(),
        child: CupertinoActivityIndicator(
          radius: 13,
        ),
      ),
    );
  }
}

class InventoryResultAPIModel {
  String errorCode;
  String errorDesc;

  InventoryResultAPIModel({this.errorCode, this.errorDesc});

  InventoryResultAPIModel.fromJson(Map<String, dynamic> data) {
    errorCode = data['ErrorCode'];
    errorDesc = data['ErrorDesc'];
  }
}

class InventorLoginyResultAPIModel {
  String errorCode;
  String errorDesc;
  String token;

  InventorLoginyResultAPIModel({this.errorCode, this.errorDesc, this.token});

  InventorLoginyResultAPIModel.fromJson(Map<String, dynamic> data) {
    errorCode = data['ErrorCode'];
    errorDesc = data['ErrorDesc'];
    token = data['Token'];
  }
}

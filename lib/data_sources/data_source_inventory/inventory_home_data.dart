import 'package:qlts/data_sources/data_source_inventory/inventory_data_config_model.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_fixed_model.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_model.dart';

class InventoryHomeData1 {
  List<Inventory> listInventory = [];
  List<InventoryFixed> listInventoryFixed = [];
  List<DataVariantConfig> listDataVariantConfig = [];
  List<DataStatusConfig> listDataStatusConfig = [];

  clear() {
    listInventory.clear();
    listInventoryFixed.clear();
    listDataVariantConfig.clear();
    listDataStatusConfig.clear();
  }
}

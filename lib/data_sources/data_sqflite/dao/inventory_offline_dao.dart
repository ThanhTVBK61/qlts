import 'package:qlts/data_sources/data_source_inventory/offline/inventory_offline_model.dart';
import 'package:qlts/data_sources/data_sqflite/dao/dao.dart';

class InventoryOfflineDao implements Dao<InventoryOffline> {
  final tableName = 'offline';
  final _columnUpdated = 'updated';
  final columnIdKqkk = 'id_kqkk';
  final _columnStt = 'stt';
  final _columnGroupVariant = 'group_variant';
  final _columnSoThe = 'so_the';
  final _columnTenThe = 'ten_the';
  final _columnLoaiCongTrinh = 'loai_cong_trinh';
  final _columnMaDuAn = 'ma_du_an';
  final _columnLoaiTs = 'loai_ts';
  final _columnGiaTri = 'gia_tri';
  final _columnMaSo = 'ma_so';
  final _columnTen = 'ten';
  final _columnSoLuong = 'so_luong';
  final _columnNamXayDung = 'nam_xay_dung';
  final _columnNamSuDung = 'nam_su_dung';
  final _columnSoTang = 'so_tang';
  final _columnDienTichSan = 'dien_tich_san';
  final _columnDiaDiem = 'dia_diem';
  final _columnMaTinhTrangSd = 'ma_tinh_trang_sd';
  final _columnTenTinhTrangSd = 'ten_tinh_trang_sd';
  final _columnMaDviSd = 'ma_dvi_sd';
  final _columnTenDviSd = 'ten_dvi_sd';
  final _columnMaPhong = 'ma_phong';
  final _columnTenPhong = 'ten_phong';
  final _columnMaCsht = 'ma_csht';
  final _columnTenCsht = 'ten_csht';
  final _columnMaNs = 'ma_ns';
  final _columnTenNs = 'ten_ns';
  final _columnQrCode = 'qr_code';
  final _columnQrCodeGiaTri = 'qr_code_gtri';
  final _columnGhiChu = 'ghi_chu';
  final _columnMaVtHhDv = 'ma_vt_hh_dv';
  final _columnNuocSx = 'nuoc_sx';
  final _columnNamSx = 'nam_sx';
  final _columnModel = 'model';
  final _columnSeri = 'seri';
  final _columnBarCode = 'barcode';
  final _columnBienKiemSoat = 'bien_kiem_soat';
  final _columnXiLanh = 'xi_lanh';
  final _columnCongSuat = 'cong_suat';
  final _columnKmSuDung = 'km_su_dung';
  final _columnLoaiNhienLieu = 'loai_nhien_lieu';
  final _columnSoKhungSoMay = 'so_khung_so_may';
  final _columnDiemDau = 'diem_dau';
  final _columnDiemCuoi = 'diem_cuoi';
  final _columnNamDauTu = 'nam_dau_tu';
  final _columnDungLuongCapDoi = 'dung_luong_cap_doi';
  final _columnDoDai = 'do_dai';
  final _columnPhuongThucLapDat = 'phuong_thuc_lap_dat';
  final _columnKcNguon = 'kc_nguon';
  final _columnIdKcNguon = 'id_kc_nguon';
  final _columnKcDich = 'kc_dich';
  final _columnIdKcDich = 'id_kc_dich';
  final _columnDungLuongOng = 'dung_luong_ong';
  final _columnDuongKinhOng = 'duong_kinh_ong';
  final _columnBeDau = 'be_dau';
  final _columnIdBeDau = 'id_be_dau';
  final _columnBeCuoi = 'be_cuoi';
  final _columnIdBeCuoi = 'id_be_cuoi';
  final _columnCotBeTongH6 = 'cot_be_tong_h6';
  final _columnCotBeTongH65 = 'cot_be_tong_h6_5';
  final _columnCotBeTongH7 = 'cot_be_tong_h7';
  final _columnCotBeTongH8 = 'cot_be_tong_h8';
  final _columnCotSat16 = 'cot_sat_l6';
  final _columnCotSat17 = 'cot_sat_l7';
  final _columnCotMuaDienLucH8 = 'cot_mua_dien_luc_h8';
  final _columnCotMuaDienLucH10 = 'cot_mua_dien_luc_h10';
  final _columnCotMuaDienLucH12 = 'cot_mua_dien_luc_h12';
  final _columnLoaiKhac = 'loai_khac';
  final _columnDiaChi = 'dia_chi';
  final _columnLoaiCot = 'loai_cot';
  final _columnDoCao = 'do_cao';
  final _columnKetCauThan = 'ket_cau_than';
  final _columnKetCauMong = 'ket_cau_mong';
  final _columnImage = 'image';

  @override
  String get createTableQuery => '''CREATE TABLE $tableName
              ($_columnUpdated INTEGER,$columnIdKqkk INTEGER PRIMARY KEY,
              $_columnStt INTEGER,
              $_columnGroupVariant TEXT,
$_columnSoThe TEXT,
$_columnTenThe TEXT,
$_columnLoaiCongTrinh TEXT,
$_columnMaDuAn TEXT,
$_columnLoaiTs TEXT,
$_columnGiaTri TEXT,
$_columnMaSo TEXT,
$_columnTen TEXT,
$_columnSoLuong TEXT,
$_columnNamXayDung TEXT,
$_columnNamSuDung TEXT,
$_columnSoTang TEXT,
$_columnDienTichSan TEXT,
$_columnDiaDiem TEXT,
$_columnMaTinhTrangSd TEXT,
$_columnTenTinhTrangSd TEXT,
$_columnMaDviSd TEXT,
$_columnTenDviSd TEXT,
$_columnMaPhong TEXT,
$_columnTenPhong TEXT,
$_columnMaCsht TEXT,
$_columnTenCsht TEXT,
$_columnMaNs TEXT,
$_columnTenNs TEXT,
$_columnQrCode TEXT,
$_columnQrCodeGiaTri TEXT,
$_columnGhiChu TEXT,
$_columnMaVtHhDv TEXT,
$_columnNuocSx TEXT,
$_columnNamSx TEXT,
$_columnModel TEXT,
$_columnSeri TEXT,
$_columnBarCode TEXT,
$_columnBienKiemSoat TEXT,
$_columnXiLanh TEXT,
$_columnCongSuat TEXT,
$_columnKmSuDung TEXT,
$_columnLoaiNhienLieu TEXT,
$_columnSoKhungSoMay TEXT,
$_columnDiemDau TEXT,
$_columnDiemCuoi TEXT,
$_columnNamDauTu TEXT,
$_columnDungLuongCapDoi TEXT,
$_columnDoDai TEXT,
$_columnPhuongThucLapDat TEXT,
$_columnKcNguon TEXT,
$_columnIdKcNguon TEXT,
$_columnKcDich TEXT,
$_columnIdKcDich TEXT,
$_columnDungLuongOng TEXT,
$_columnDuongKinhOng TEXT,
$_columnBeDau TEXT,
$_columnIdBeDau TEXT,
$_columnBeCuoi TEXT,
$_columnIdBeCuoi TEXT,
$_columnCotBeTongH6 TEXT,
$_columnCotBeTongH65 TEXT,
$_columnCotBeTongH7 TEXT,
$_columnCotBeTongH8 TEXT,
$_columnCotSat16 TEXT,
$_columnCotSat17 TEXT,
$_columnCotMuaDienLucH8 TEXT,
$_columnCotMuaDienLucH10 TEXT,
$_columnCotMuaDienLucH12 TEXT,
$_columnLoaiKhac TEXT,
$_columnDiaChi TEXT,
$_columnLoaiCot TEXT,
$_columnDoCao TEXT,
$_columnKetCauThan TEXT,
$_columnKetCauMong TEXT,
$_columnImage TEXT
        )''';

  @override
  List<InventoryOffline> fromList(List<Map<String, dynamic>> query) {
    List<InventoryOffline> listInventoryOffline = List<InventoryOffline>();
    for (Map map in query) {
      listInventoryOffline.add(fromMap(map));
    }
    return listInventoryOffline;
  }

  @override
  InventoryOffline fromMap(Map<String, dynamic> query) {
    InventoryOffline inventoryOffline = InventoryOffline();
    inventoryOffline.idKqkk = query[columnIdKqkk];
    inventoryOffline.stt = query[_columnStt];
    inventoryOffline.groupVariant = query[_columnGroupVariant];
    inventoryOffline.soThe = query[_columnSoThe];
    inventoryOffline.tenThe = query[_columnTenThe];
    inventoryOffline.loaiCongTrinh = query[_columnLoaiCongTrinh];
    inventoryOffline.maDuAn = query[_columnMaDuAn];
    inventoryOffline.loaiTs = query[_columnLoaiTs];
    inventoryOffline.giaTri = query[_columnGiaTri];
    inventoryOffline.maSo = query[_columnMaSo];
    inventoryOffline.ten = query[_columnTen];
    inventoryOffline.soLuong = query[_columnSoLuong];
    inventoryOffline.namXayDung = query[_columnNamXayDung];
    inventoryOffline.namSuDung = query[_columnNamSuDung];
    inventoryOffline.soTang = query[_columnSoTang];
    inventoryOffline.dienTichSan = query[_columnDienTichSan];
    inventoryOffline.diaDiem = query[_columnDiaDiem];
    inventoryOffline.maTinhTrangSd = query[_columnMaTinhTrangSd];
    inventoryOffline.tenTinhTrangSd = query[_columnTenTinhTrangSd];
    inventoryOffline.maDviSd = query[_columnMaDviSd];
    inventoryOffline.tenDviSd = query[_columnTenDviSd];
    inventoryOffline.maPhong = query[_columnMaPhong];
    inventoryOffline.tenPhong = query[_columnTenPhong];
    inventoryOffline.maCsht = query[_columnMaCsht];
    inventoryOffline.tenCsht = query[_columnTenCsht];
    inventoryOffline.maNs = query[_columnMaNs];
    inventoryOffline.tenNs = query[_columnTenNs];
    inventoryOffline.qrCode = query[_columnQrCode];
    inventoryOffline.qrCodeGiaTri = query[_columnQrCodeGiaTri];
    inventoryOffline.ghiChu = query[_columnGhiChu];
    inventoryOffline.maVtHhDv = query[_columnMaVtHhDv];
    inventoryOffline.nuocSx = query[_columnNuocSx];
    inventoryOffline.namSx = query[_columnNamSx];
    inventoryOffline.model = query[_columnModel];
    inventoryOffline.seri = query[_columnSeri];
    inventoryOffline.barCode = query[_columnBarCode];
    inventoryOffline.bienKiemSoat = query[_columnBienKiemSoat];
    inventoryOffline.xiLanh = query[_columnXiLanh];
    inventoryOffline.congSuat = query[_columnCongSuat];
    inventoryOffline.kmSuDung = query[_columnKmSuDung];
    inventoryOffline.loaiNhienLieu = query[_columnLoaiNhienLieu];
    inventoryOffline.soKhungSoMay = query[_columnSoKhungSoMay];
    inventoryOffline.diemDau = query[_columnDiemDau];
    inventoryOffline.diemCuoi = query[_columnDiemCuoi];
    inventoryOffline.namDauTu = query[_columnNamDauTu];
    inventoryOffline.dungLuongCapDoi = query[_columnDungLuongCapDoi];
    inventoryOffline.doDai = query[_columnDoDai];
    inventoryOffline.phuongThucLapDat = query[_columnPhuongThucLapDat];
    inventoryOffline.kcNguon = query[_columnKcNguon];
    inventoryOffline.idKcNguon = query[_columnIdKcNguon];
    inventoryOffline.kcDich = query[_columnKcDich];
    inventoryOffline.idKcDich = query[_columnIdKcDich];
    inventoryOffline.dungLuongOng = query[_columnDungLuongOng];
    inventoryOffline.duongKinhOng = query[_columnDuongKinhOng];
    inventoryOffline.beDau = query[_columnBeDau];
    inventoryOffline.idBeDau = query[_columnIdBeDau];
    inventoryOffline.beCuoi = query[_columnBeCuoi];
    inventoryOffline.idBeCuoi = query[_columnIdBeCuoi];
    inventoryOffline.cotBeTongH6 = query[_columnCotBeTongH6];
    inventoryOffline.cotBeTongH65 = query[_columnCotBeTongH65];
    inventoryOffline.cotBeTongH7 = query[_columnCotBeTongH7];
    inventoryOffline.cotBeTongH8 = query[_columnCotBeTongH8];
    inventoryOffline.cotSat16 = query[_columnCotSat16];
    inventoryOffline.cotSat17 = query[_columnCotSat17];
    inventoryOffline.cotMuaDienLucH8 = query[_columnCotMuaDienLucH8];
    inventoryOffline.cotMuaDienLucH10 = query[_columnCotMuaDienLucH10];
    inventoryOffline.cotMuaDienLucH12 = query[_columnCotMuaDienLucH12];
    inventoryOffline.loaiKhac = query[_columnLoaiKhac];
    inventoryOffline.diaChi = query[_columnDiaChi];
    inventoryOffline.loaiCot = query[_columnLoaiCot];
    inventoryOffline.doCao = query[_columnDoCao];
    inventoryOffline.ketCauThan = query[_columnKetCauThan];
    inventoryOffline.ketCauMong = query[_columnKetCauMong];
    inventoryOffline.image = query[_columnImage];

    return inventoryOffline;
  }

  // @override
  // InventoryOffline fromMap(Map<String, dynamic> query) {
  //   InventoryOffline inventoryOffline = InventoryOffline();
  //
  //   inventoryOffline.idKqkk = query[columnIdKqkk];
  //   inventoryOffline.groupVariant = query[_columnGroupVariant];
  //   inventoryOffline.soThe = query[_columnSoThe];
  //   inventoryOffline.maSo = query[_columnMaSo];
  //   inventoryOffline.qrCodeGiaTri = query[_columnQrCodeGiaTri];
  //   inventoryOffline.seri = query[_columnSeri];
  //   inventoryOffline.barCode = query[_columnBarCode];
  //   inventoryOffline.data = query[_columnData];
  //
  //   return inventoryOffline;
  // }

  ///toMap
  @override
  Map<String, dynamic> toMap(InventoryOffline object) {
    return <String, dynamic>{
      _columnUpdated: object.updated ?? 0,
      columnIdKqkk: object.idKqkk ?? '',
      _columnStt: object.stt ?? -1,
      _columnGroupVariant: object.groupVariant ?? '',
      _columnSoThe: object.soThe ?? '',
      _columnTenThe: object.tenThe ?? '',
      _columnLoaiCongTrinh: object.loaiCongTrinh ?? '',
      _columnMaDuAn: object.maDuAn ?? '',
      _columnLoaiTs: object.loaiTs ?? '',
      _columnGiaTri: object.giaTri ?? '',
      _columnMaSo: object.maSo ?? '',
      _columnTen: object.ten ?? '',
      _columnSoLuong: object.soLuong ?? '',
      _columnNamXayDung: object.namXayDung ?? '',
      _columnNamSuDung: object.namSuDung ?? '',
      _columnSoTang: object.soTang ?? '',
      _columnDienTichSan: object.dienTichSan ?? '',
      _columnDiaDiem: object.diaDiem ?? '',
      _columnMaTinhTrangSd: object.maTinhTrangSd ?? '',
      _columnTenTinhTrangSd: object.tenTinhTrangSd ?? '',
      _columnMaDviSd: object.maDviSd ?? '',
      _columnTenDviSd: object.tenDviSd ?? '',
      _columnMaPhong: object.maPhong ?? '',
      _columnTenPhong: object.tenPhong ?? '',
      _columnMaCsht: object.maCsht ?? '',
      _columnTenCsht: object.tenCsht ?? '',
      _columnMaNs: object.maNs ?? '',
      _columnTenNs: object.tenNs ?? '',
      _columnQrCode: object.qrCode ?? '',
      _columnQrCodeGiaTri: object.qrCodeGiaTri ?? '',
      _columnGhiChu: object.ghiChu ?? '',
      _columnMaVtHhDv: object.maVtHhDv ?? '',
      _columnNuocSx: object.nuocSx ?? '',
      _columnNamSx: object.namSx ?? '',
      _columnModel: object.model ?? '',
      _columnSeri: object.seri ?? '',
      _columnBarCode: object.barCode ?? '',
      _columnBienKiemSoat: object.bienKiemSoat ?? '',
      _columnXiLanh: object.xiLanh ?? '',
      _columnCongSuat: object.congSuat ?? '',
      _columnKmSuDung: object.kmSuDung ?? '',
      _columnLoaiNhienLieu: object.loaiNhienLieu ?? '',
      _columnSoKhungSoMay: object.soKhungSoMay ?? '',
      _columnDiemDau: object.diemDau ?? '',
      _columnDiemCuoi: object.diemCuoi ?? '',
      _columnNamDauTu: object.namDauTu ?? '',
      _columnDungLuongCapDoi: object.dungLuongCapDoi ?? '',
      _columnDoDai: object.doDai ?? '',
      _columnPhuongThucLapDat: object.phuongThucLapDat ?? '',
      _columnKcNguon: object.kcNguon ?? '',
      _columnIdKcNguon: object.idKcNguon ?? '',
      _columnKcDich: object.kcDich ?? '',
      _columnIdKcDich: object.idKcDich ?? '',
      _columnDungLuongOng: object.dungLuongOng ?? '',
      _columnDuongKinhOng: object.duongKinhOng ?? '',
      _columnBeDau: object.beDau ?? '',
      _columnIdBeDau: object.idBeDau ?? '',
      _columnBeCuoi: object.beCuoi ?? '',
      _columnIdBeCuoi: object.idBeCuoi ?? '',
      _columnCotBeTongH6: object.cotBeTongH6 ?? '',
      _columnCotBeTongH65: object.cotBeTongH65 ?? '',
      _columnCotBeTongH7: object.cotBeTongH7 ?? '',
      _columnCotBeTongH8: object.cotBeTongH8 ?? '',
      _columnCotSat16: object.cotSat16 ?? '',
      _columnCotSat17: object.cotSat17 ?? '',
      _columnCotMuaDienLucH8: object.cotMuaDienLucH8 ?? '',
      _columnCotMuaDienLucH10: object.cotMuaDienLucH10 ?? '',
      _columnCotMuaDienLucH12: object.cotMuaDienLucH12 ?? '',
      _columnLoaiKhac: object.loaiKhac ?? '',
      _columnDiaChi: object.diaChi ?? '',
      _columnLoaiCot: object.loaiCot ?? '',
      _columnDoCao: object.doCao ?? '',
      _columnKetCauThan: object.ketCauThan ?? '',
      _columnKetCauMong: object.ketCauMong ?? '',
      _columnImage: object.image ?? ''
    };
  }

  // @override
  // Map<String, dynamic> toMap(InventoryOffline object) {
  //   return <String, dynamic>{
  //     columnIdKqkk: object.idKqkk,
  //     _columnGroupVariant: object.groupVariant ?? '',
  //     _columnSoThe: object.soThe ?? '',
  //     _columnMaSo: object.maSo ?? '',
  //     _columnQrCodeGiaTri: object.qrCodeGiaTri ?? '',
  //     _columnSeri: object.seri ?? '',
  //     _columnBarCode: object.barCode ?? '',
  //     _columnData: object.data ?? '',
  //   };
  // }

  List<Map> toListUpload(List<Map<String, dynamic>> query) {
    List<Map> data = [];
    for (Map map in query) {
      data.add(fromMapUpload(map));
    }

    return data;
  }

  Map<String, dynamic> fromMapUpload(Map<String, dynamic> query) {
    return <String, dynamic>{
      'updated': query[_columnUpdated],
      'id_kqkk': query[columnIdKqkk],
      'stt': query[_columnStt],
      'group_variant': query[_columnGroupVariant],
      'so_the': query[_columnSoThe],
      'ten_the': query[_columnTenThe],
      'loai_cong_trinh': query[_columnLoaiCongTrinh],
      'ma_du_an': query[_columnMaDuAn],
      'loai_ts': query[_columnLoaiTs],
      'ma_so': query[_columnMaSo],
      'ten': query[_columnTen],
      'so_luong': query[_columnSoLuong],
      'nam_xay_dung': query[_columnNamXayDung],
      'nam_su_dung': query[_columnNamSuDung],
      'so_tang': query[_columnSoTang],
      'dien_tich_san': query[_columnDienTichSan],
      'dia_diem': query[_columnDiaDiem],
      'ma_tinh_trang_sd': query[_columnMaTinhTrangSd],
      'ten_tinh_trang_sd': query[_columnTenTinhTrangSd],
      'ma_dvi_sd': query[_columnMaDviSd],
      'ten_dvi_sd': query[_columnTenTinhTrangSd],
      'ma_phong': query[_columnMaPhong],
      'ten_phong': query[_columnTenPhong],
      'ma_csht': query[_columnMaCsht],
      'ten_csht': query[_columnTenCsht],
      'ma_ns': query[_columnMaNs],
      'ten_ns': query[_columnTenNs],
      'qr_code': query[_columnQrCode],
      'qr_code_gtri': query[_columnQrCodeGiaTri],
      'ghi_chu': query[_columnGhiChu],
      'ma_vt_hh_dv': query[_columnMaVtHhDv],
      'nuoc_sx': query[_columnNuocSx],
      'nam_sx': query[_columnNamSx],
      'model': query[_columnModel],
      'seri': query[_columnSeri],
      'barcode': query[_columnBarCode],
      'bien_kiem_soat': query[_columnBienKiemSoat],
      'xi_lanh': query[_columnXiLanh],
      'cong_suat': query[_columnCongSuat],
      'km_su_dung': query[_columnKmSuDung],
      'loai_nhien_lieu': query[_columnLoaiNhienLieu],
      'so_khung_so_may': query[_columnSoKhungSoMay],
      'diem_dau': query[_columnDiemDau],
      'diem_cuoi': query[_columnDiemCuoi],
      'nam_dau_tu': query[_columnNamDauTu],
      'dung_luong_cap_doi': query[_columnDungLuongCapDoi],
      'do_dai': query[_columnDoDai],
      'phuong_thuc_lap_dat': query[_columnPhuongThucLapDat],
      'kc_nguon': query[_columnKcNguon],
      'id_kc_nguon': query[_columnIdKcNguon],
      'kc_dich': query[_columnKcDich],
      'id_kc_dich': query[_columnIdKcDich],
      'dung_luong_ong': query[_columnDungLuongOng],
      'duong_kinh_ong': query[_columnDuongKinhOng],
      'be_dau': query[_columnBeDau],
      'id_be_dau': query[_columnIdBeDau],
      'be_cuoi': query[_columnBeCuoi],
      'id_be_cuoi': query[_columnIdBeCuoi],
      'cot_be_tong_h6': query[_columnCotBeTongH6],
      'cot_be_tong_h6_5': query[_columnCotBeTongH65],
      'cot_be_tong_h7': query[_columnCotBeTongH7],
      'cot_be_tong_h8': query[_columnCotBeTongH8],
      'cot_sat_16': query[_columnCotSat16],
      'cot_sat_17': query[_columnCotSat17],
      'cot_mua_dien_luc_h8': query[_columnCotMuaDienLucH8],
      'cot_mua_dien_luc_h10': query[_columnCotMuaDienLucH10],
      'cot_mua_dien_luc_h12': query[_columnCotMuaDienLucH12],
      'loai_khac': query[_columnLoaiKhac],
      'dia_chi': query[_columnDiaChi],
      'loai_cot': query[_columnLoaiCot],
      'do_cao': query[_columnDoCao],
      'ket_cau_than': query[_columnKetCauThan],
      'ket_cau_mong': query[_columnKetCauMong],
      'image': query[_columnImage]
    };
  }
}

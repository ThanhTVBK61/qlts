abstract class DatabaseRepository<T> {
  Future<T> insert(T object);

  Future<int> update(T object);

  Future<T> delete(T object);

  Future insertBatch(List<T> objects);

  Future<Map<String, dynamic>> getData();

  Future<void> clear();
}

// import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_image_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';
import 'package:qlts/provider/home_search/search_data_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/widget_splash/splash_page.dart';

// List<CameraDescription> cameras = [];

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  //
  // ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
  // connectionStatus.initialize();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ///inventory
        ChangeNotifierProvider(create: (_) => InventoryData()),
        ChangeNotifierProvider(create: (_) => InventoryFixedDataProvider()),
        ChangeNotifierProvider(create: (_) => InventoryConfigData()),
        ChangeNotifierProvider(create: (_) => InventoryDetailData()),
        ChangeNotifierProvider(create: (_) => InventoryImageProvider()),

        ///search
        ChangeNotifierProvider(create: (_) => SearchDataModel()),

        ///input data
        ChangeNotifierProvider(create: (_) => HomeDataInputProvider()),

        ChangeNotifierProvider(create: (_) => QRCodeProvider()),
        ChangeNotifierProvider(create: (_) => TabProvider()),
      ],
      child: ScreenUtilInit(
        designSize: Size(1440, 2880),
        allowFontScaling: false,
        builder: ()=> MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            // visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: SplashPage(),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/widget_home/button_widget.dart';
import 'package:qlts/widget_notification/notification_model.dart';

class MyNotification extends StatefulWidget {

  const MyNotification({Key key}): super(key: key);

  @override
  State<StatefulWidget> createState() => MyNotificationState();

}

class MyNotificationState extends State<MyNotification> {
  List<NotificationModel> notifications = [
    new NotificationModel(
        title: 'Thông báo cập nhật chức năng mới',
        content:
            'Hệ thống quản lý tài sản đã thực hiện cập nhật tính năng.Hệ thống quản lý tài sản đã thực hiện cập nhật tính năng.Hệ thống quản lý tài sản đã thực hiện cập nhật tính năng.',
        time: '20/11/2020 10:11:22')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text('Danh sách thông báo'),
          ),
        ),
        body: Center(
            child: RaisedGradientButton(
          child: Text('Coming soon',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * fs)),
        )));
  }
}

class MyDetailNotification extends StatelessWidget {
  final title;

  MyDetailNotification(this.title);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Center(child: Text(title))),
      body: Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        decoration:
            BoxDecoration(border: Border.all(color: Colors.black, width: 1.0)),
      ),
    );
  }
}

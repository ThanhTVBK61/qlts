import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_result_API_model.dart';
import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/repository/repository_database.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_home/button_widget.dart';
import 'package:qlts/widget_login/my_login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Logout extends StatefulWidget {

  const Logout({Key key}): super(key: key);

  @override
  State<StatefulWidget> createState() => LogoutState();
}

class LogoutState extends State<Logout> {
  Repository repository = Repository();
  RepositoryDatabase repositoryDatabase =
      RepositoryDatabase(DatabaseProvider.getInstance);
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  String token;

  Future<String> getToken() async {
    SharedPreferences prefs = await _prefs;
    return (prefs.getString('token')) ?? '';
  }

  @override
  Widget build(BuildContext context) {
    var getInventoryData = Provider.of<InventoryData>(context);
    var getInventoryConfigData = Provider.of<InventoryConfigData>(context);
    var getInventoryFixedData =
        Provider.of<InventoryFixedDataProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Tài khoản')),
      ),
      body: Center(
        child: RaisedGradientButton(
          child: Text('Đăng xuất',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * fs)),
          onPressed: () {
            showDialog(
                context: context,
                builder: (_) => CupertinoAlertDialog(
                      title: Text('Thông báo',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 14 * fs)),
                      content: Text('Bạn có chắc chắn muốn đăng xuất?',
                          style: TextStyle(
                              color: Colors.black, fontSize: 14 * fs)),
                      actions: [
                        FlatButton(
                          child: Text('Đóng',
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14 * fs)),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                        ),
                        FlatButton(
                          child: Text('Đăng xuất',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14 * fs)),
                          onPressed: () async {
                            Navigator.of(context, rootNavigator: true).pop();
                            showLoaderDialog(context);

                            if(token == null){
                              token = await getToken();
                            }
                            InventoryResultAPIModel inventoryResultAPIModel =
                                await repository.fetchLogout(
                                    token: token);
                            if (inventoryResultAPIModel.errorCode != SUCCESS) {
                              if (inventoryResultAPIModel.errorCode ==
                                  TIME_OUT) {
                                getInventoryData.clear();
                                getInventoryFixedData.clear();
                                getInventoryConfigData.clear();
                              }
                              Navigator.of(context, rootNavigator: true).pop();
                              showErrorDialog(
                                  context: this.context,
                                  errorCode: inventoryResultAPIModel.errorCode,
                                  errorDesc: inventoryResultAPIModel.errorDesc);
                            } else {
                              getInventoryData.clear();
                              getInventoryFixedData.clear();
                              getInventoryConfigData.clear();

                              ///Clear token
                              print('****Token Timeout**set token ='
                                  '******');
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              prefs.setString('token', '');

                              ///Clear sqflite
                              // repositoryDatabase.databaseRepositoryStatus
                              //     .clear();
                              // repositoryDatabase.databaseRepositoryVariant
                              //     .clear();
                              repositoryDatabase.databaseRepositoryOffline
                                  .clear();

                              Navigator.of(context, rootNavigator: true).pop();

                              Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
                                  CupertinoPageRoute(
                                      builder: (context) => MyLoginPage(
                                            url: URL_LOGOUT,
                                          )),
                                  (Route<dynamic> route) => false);
                            }
                          },
                        )
                      ],
                    ));
          },
        ),
      ),
    );
  }
}

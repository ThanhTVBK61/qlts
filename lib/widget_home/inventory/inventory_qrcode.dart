import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:qlts/animating_route_transition.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_detail_model.dart';
import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';

// import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/repository/repository_database.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_home/button_widget.dart';
import 'package:qlts/widget_home/home_qrcode/permission.dart';
import 'package:qlts/widget_home/inventory/capture_image.dart';
import 'package:qlts/widget_home/inventory/inventory_information_update.dart';

class QRCodeInventory extends StatefulWidget {
  final token;

  QRCodeInventory({this.token});

  @override
  State<StatefulWidget> createState() => QRCodeInventoryState();
}

class QRCodeInventoryState extends State<QRCodeInventory>
    with AutomaticKeepAliveClientMixin<QRCodeInventory> {
  Repository repository = Repository();
  RepositoryDatabase repositoryDatabase =
      RepositoryDatabase(DatabaseProvider.getInstance);

  List<InventoryDetail> listInventoryDetail = [];

  final picker = ImagePicker();
  InventoryDetailModel inventoryDetailModel;

  var getQRCodeProvider;
  var getInventoryDetailData;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    getQRCodeProvider = Provider.of<QRCodeProvider>(context);
    getInventoryDetailData = Provider.of<InventoryDetailData>(context);

    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ///get du lieu tu camera
          Card(
            margin: EdgeInsets.only(
                right: 15 * ws, left: 15 * ws, bottom: 10 * ws, top: 15 * ws),
            shadowColor: Colors.white70,
            elevation: 10.0,
            color: Colors.white.withOpacity(1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16 * ws),
            ),
            child: Container(
                padding: EdgeInsets.all(10 * ws),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                        child: PermissionWidget(
                            token: widget.token,
                            isSearch: false,
                            permission: Permission.camera)),
                    Visibility(
                      visible: (getQRCodeProvider.data == '') ? false : true,
                      child: Container(
                          margin: EdgeInsets.only(
                              top: 10 * ws, left: 15 * ws, right: 5 * ws),
                          child: Text(getQRCodeProvider.data ?? '')),
                    ),
                    Visibility(
                      visible:
                          // getInventoryData.online &&
                          (getInventoryDetailData.listInventoryDetail.length > 0
                              ? true
                              : false),
                      child: RaisedGradientButton(
                        width: 160 * ws,
                        marginTop: 10 * ws,
                        child: Text(CHUP_ANH,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 14 * fs)),
                        onPressed: () async {
                          ///Request location, camera permissions
                          Map<Permission, PermissionStatus> statuses = await [
                            Permission.camera,
                            Permission.location,
                          ].request();
                          String request = 'Vui lòng cấp quyền ';
                          if (statuses[Permission.camera] ==
                                  PermissionStatus.permanentlyDenied ||
                              statuses[Permission.camera] ==
                                  PermissionStatus.denied) {
                            request += 'Camera ';
                          }
                          if (statuses[Permission.location] ==
                                  PermissionStatus.permanentlyDenied ||
                              statuses[Permission.location] ==
                                  PermissionStatus.denied) {
                            request += 'Vị trí ';
                          }

                          if (statuses[Permission.camera] ==
                                  PermissionStatus.granted &&
                              statuses[Permission.location] ==
                                  PermissionStatus.granted) {
                            ///
                            ///Chụp ảnh hiện trường
                            ///
                            showLoaderDialog(context);
                            String address = await getCurrentAddress();
                            Navigator.of(context, rootNavigator: true).pop();
                            String image = await getImage();

                            Navigator.push(
                                context,
                                RouteTransition(
                                    widget: TakePictureScreen(
                                  image: image,
                                  address: address,
                                )));
                          } else {
                            await showErrorDialog(
                                context: context, errorDesc: request);
                          }
                        },
                      ),
                    ),
                  ],
                )),
          ),

          InventoryInformationUpdate(token: widget.token)
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}

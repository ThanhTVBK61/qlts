import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:qlts/animating_route_transition.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_detail_model.dart';
import 'package:qlts/data_sources/data_source_search/item_search_model.dart';
import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_image_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/repository/repository_database.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_home/card_input.dart';
import 'package:qlts/widget_home/inventory/capture_image.dart';
import 'package:qlts/widget_home/inventory/inventory_information_update.dart';
import 'package:qlts/repository/repository.dart';

/// Widget nhập dữ liệu kiểm kê tài sản

class InventoryInputData extends StatefulWidget {
  final token;

  InventoryInputData({this.token});

  @override
  State<StatefulWidget> createState() => InventoryInputDataState();
}

class InventoryInputDataState extends State<InventoryInputData>
    with AutomaticKeepAliveClientMixin<InventoryInputData> {
  ItemSearchModel itemSearchModel;
  Repository repository = Repository();
  RepositoryDatabase repositoryDatabase =
      RepositoryDatabase(DatabaseProvider.getInstance);

  InventoryDetailModel inventoryDetailModel;

  final picker = ImagePicker();

  var getInventoryData;
  var getInventoryDetailData;
  var getInventoryImageProvider;
  var getHomeDataInputProvider;
  var getInventoryFixedDataProvider;
  var getQRCodeProvider;
  var getInventoryConfigData;
  var getTabProvider;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    getInventoryData = Provider.of<InventoryData>(context);
    getInventoryDetailData =
        Provider.of<InventoryDetailData>(context, listen: false);
    getInventoryImageProvider =
        Provider.of<InventoryImageProvider>(context, listen: false);
    getHomeDataInputProvider =
        Provider.of<HomeDataInputProvider>(context, listen: false);
    getInventoryFixedDataProvider =
        Provider.of<InventoryFixedDataProvider>(context, listen: false);
    getQRCodeProvider = Provider.of<QRCodeProvider>(context, listen: false);

    ///Get Config
    getInventoryConfigData =
        Provider.of<InventoryConfigData>(context, listen: false);
    getTabProvider = Provider.of<TabProvider>(context, listen: false);

    return SingleChildScrollView(
      child: Column(
        children: [
          CardInput(
              isSearch: false,
              onCapturePress: () async {
                Map<Permission, PermissionStatus> statuses = await [
                  Permission.camera,
                  Permission.location,
                ].request();
                String request = 'Vui lòng cấp quyền ';
                if (statuses[Permission.camera] ==
                        PermissionStatus.permanentlyDenied ||
                    statuses[Permission.camera] == PermissionStatus.denied) {
                  request += 'Camera ';
                }
                if (statuses[Permission.location] ==
                        PermissionStatus.permanentlyDenied ||
                    statuses[Permission.location] == PermissionStatus.denied) {
                  request += 'Vị trí ';
                }

                if (statuses[Permission.camera] == PermissionStatus.granted &&
                    statuses[Permission.location] == PermissionStatus.granted) {
                  ///
                  ///Chụp ảnh hiện trường
                  ///
                  showLoaderDialog(context);
                  String address = await getCurrentAddress();
                  Navigator.of(context, rootNavigator: true).pop();
                  String image = await getImage();

                  Navigator.push(
                      context,
                      RouteTransition(
                          widget: TakePictureScreen(
                        image: image,
                        address: address,
                      )));
                } else {
                  showErrorDialog(context: context, errorDesc: request);
                }
              },
              onSearchPress: () async {
                if (!getHomeDataInputProvider.validate()) {
                  getHomeDataInputProvider.updateError(true);
                } else {
                  FocusScope.of(context).requestFocus(FocusNode());

                  ///Clear old data
                  getInventoryDetailData.clear();

                  if (getInventoryData.online) {
                    getInventoryImageProvider.clear();

                    showLoaderDialog(context);

                    if (getHomeDataInputProvider.type == 1) {
                      print(
                          '****inventory_input_data*** token: ${widget.token},inventoryId: ${getInventoryData.currentId.toString()},soThe: ${getHomeDataInputProvider.soThe}, assetCodeNumber: ${getHomeDataInputProvider.id},*********');
                      inventoryDetailModel =
                          await repository.fetchInventoryDetailInput(
                              token: widget.token,
                              inventoryId:
                                  getInventoryData.currentId.toString(),
                              soThe: getHomeDataInputProvider.soThe.trim(),
                              assetCodeNumber:
                                  getHomeDataInputProvider.id.trim());
                    } else if (getHomeDataInputProvider.type == 2) {
                      print(
                          '****inventory_input_data serial*** token: ${widget.token},inventoryId: ${getInventoryData.currentId.toString()},serial: ${getHomeDataInputProvider.serial}*********');
                      inventoryDetailModel =
                          await repository.fetchInventoryDetailInputSerial(
                              token: widget.token,
                              inventoryId:
                                  getInventoryData.currentId.toString(),
                              serial: getHomeDataInputProvider.serial.trim());
                    } else if (getHomeDataInputProvider.type == 3) {
                      print(
                          '****inventory_input_data qrcode*** token: ${widget.token},inventoryId: ${getInventoryData.currentId.toString()},qrcode: ${getHomeDataInputProvider.qrcode}*********');
                      inventoryDetailModel =
                          await repository.fetchInventoryDetailQRCode(
                              token: widget.token,
                              inventoryId:
                                  getInventoryData.currentId.toString(),
                              typeCode: 'qrcode',
                              value: getHomeDataInputProvider.qrcode.trim());
                    }

                    if (inventoryDetailModel.errorCode != SUCCESS) {
                      if (inventoryDetailModel.errorCode == TIME_OUT) {
                        getInventoryData.clear();
                        getInventoryFixedDataProvider.clear();
                        getInventoryConfigData.clear();
                        getInventoryDetailData.clear();
                        getInventoryImageProvider.clear();
                        getHomeDataInputProvider.clear();
                        getQRCodeProvider.clear();
                        getTabProvider.clear();
                      }
                      Navigator.of(context, rootNavigator: true).pop();

                      ///Show error code
                      showErrorDialog(
                          context: this.context,
                          errorCode: inventoryDetailModel.errorCode,
                          errorDesc: inventoryDetailModel.errorDesc);
                    } else {
                      getInventoryDetailData.update(inventoryDetailModel);
                      Navigator.of(context, rootNavigator: true).pop();
                    }
                  } else {
                    getInventoryImageProvider.clear();
                    int countRow = await repositoryDatabase
                        .databaseRepositoryOffline
                        .countRow();

                    if (countRow <= 0) {
                      showErrorDialog(
                          context: context, errorDesc: EMPTY_SQFLITE);
                    } else {
                      dynamic inventoryOffline;
                      showLoaderDialog(context);
                      if (getHomeDataInputProvider.type == 1) {
                        inventoryOffline = await repositoryDatabase
                            .databaseRepositoryOffline
                            .getDataById(
                                getHomeDataInputProvider.soThe.trim(),
                                getHomeDataInputProvider.id.trim(),
                                getInventoryConfigData.listDataVariantConfig);
                      } else if (getHomeDataInputProvider.type == 2) {
                        inventoryOffline = await repositoryDatabase
                            .databaseRepositoryOffline
                            .getDataBySerial(
                                getHomeDataInputProvider.serial.trim(),
                                getInventoryConfigData.listDataVariantConfig);
                      } else if (getHomeDataInputProvider.type == 3) {
                        inventoryOffline = await repositoryDatabase
                            .databaseRepositoryOffline
                            .getDataByQRCode(
                                getHomeDataInputProvider.qrcode.trim(),
                                getInventoryConfigData.listDataVariantConfig);
                      }

                      print('---------inventoryOffline-----------');
                      print(inventoryOffline);
                      if (inventoryOffline != null) {
                        print(getInventoryConfigData
                            .listDataVariantConfig[0].code);
                        print('-----Then--Update-----------');
                        getInventoryDetailData
                            .updateInventoryOffline(inventoryOffline);
                        getInventoryImageProvider
                            .update(inventoryOffline.image);
                        Navigator.of(context, rootNavigator: true).pop();
                      } else {
                        Navigator.of(context, rootNavigator: true).pop();
                        showErrorDialog(
                            context: context, errorDesc: ERROR_NOT_FOUND);
                      }
                    }
                  }
                }
              }),
          InventoryInformationUpdate(token: widget.token)
        ],
      ),
      // ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

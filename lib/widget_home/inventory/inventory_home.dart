import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_image_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/widget_BG/widget_background.dart';
import 'package:qlts/widget_home/inventory/inventory_input_data.dart';
import 'package:qlts/widget_home/inventory/inventory_qrcode.dart';
import 'package:qlts/widget_home/inventory/inventory_type.dart';

class HomeInventory extends StatefulWidget {
  final token;

  HomeInventory({this.token});

  @override
  State<StatefulWidget> createState() => HomeInventoryState();
}

class HomeInventoryState extends State<HomeInventory>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var getInventoryData = Provider.of<InventoryData>(context, listen: false);
    var getInventoryFixedDataProvider =
        Provider.of<InventoryFixedDataProvider>(context, listen: false);
    var getInventoryConfigData =
        Provider.of<InventoryConfigData>(context, listen: false);
    var getInventoryDetailData =
        Provider.of<InventoryDetailData>(context, listen: false);
    var getInventoryImageProvider =
        Provider.of<InventoryImageProvider>(context, listen: false);

    var getHomeDataInputProvider =
        Provider.of<HomeDataInputProvider>(context, listen: false);
    var getQRCodeProvider = Provider.of<QRCodeProvider>(context, listen: false);
    var getTabProvider = Provider.of<TabProvider>(context, listen: false);

    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(color: Colors.white),
        ),
        backgroundImage(context),
        DefaultTabController(
          length: 3,
          child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                  shape: Border(bottom: BorderSide(color: Colors.white)),
                  title: Container(
                      padding: EdgeInsets.only(top: 10 * ws),
                      child: Text(
                        TITLE_INVENTORY,
                        style: TextStyle(
                            color: Color(0xFFF2F2F9), fontSize: 20 * fs),
                      )),
                  centerTitle: true,
                  backgroundColor: Colors.transparent,
                  toolbarHeight: 110 * ws,
                  elevation: 0.0,
                  leading: Container(
                    margin: EdgeInsets.only(left: 20 * ws),
                    child: IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        getInventoryData.clear();
                        getInventoryFixedDataProvider.clear();
                        getInventoryConfigData.clear();
                        getInventoryDetailData.clear();
                        getInventoryImageProvider.clear();
                        getHomeDataInputProvider.clear();
                        getQRCodeProvider.clear();
                        getTabProvider.clear();

                        Navigator.pop(context);
                      },
                    ),
                  ),
                  bottom: PreferredSize(
                    preferredSize:
                        Size(MediaQuery.of(context).size.width, 40 * ws),
                    child: TabBar(
                        indicatorWeight: 3,
                        onTap: (index) {
                          getTabProvider.updateIndex(index);
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        tabs: [
                          Container(
                              height: 40 * ws,
                              width: double.infinity,
                              child: Center(
                                  child: Text(
                                TABBAR_TITLE_INVENTORY_HOME,
                                style: TextStyle(fontSize: 15 * fs),
                              ))),
                          Container(
                              height: 40 * ws,
                              child: Center(
                                  child: Text(
                                TABBAR_TITLE_QRCODE,
                                style: TextStyle(fontSize: 15 * fs),
                              ))),
                          Container(
                              height: 40 * ws,
                              child: Center(
                                  child: Text(
                                TABBAR_TITLE_INPUT,
                                style: TextStyle(fontSize: 15 * fs),
                              ))),
                        ],
                        indicatorColor: Colors.white),
                  )),
              body: TabBarView(
                children: [
                  InventoryType(token: widget.token),
                  QRCodeInventory(token: widget.token),
                  InventoryInputData(token: widget.token),
                ],
              )),
        )
      ],
    );
  }
}

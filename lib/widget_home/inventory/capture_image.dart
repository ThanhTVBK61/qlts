// A screen that allows users to take a picture using a given camera.
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/provider/home_inventory/inventory_image_provider.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';

class TakePictureScreen extends StatefulWidget {

  final image;
  final address;

  TakePictureScreen({@required this.image, @required this.address});

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {

  GlobalKey _globalKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    var getInventoryImage = Provider.of<InventoryImageProvider>(context);

    DateTime now = DateTime.now();
    String formattedTime = DateFormat('HH:mm:ss').format(now);
    String formattedDate = DateFormat('dd - MM - yyyy').format(now);
    String formattedDateTime =
        'Thời gian:   $formattedTime\nNgày       :   $formattedDate';

    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Ảnh hiện trường')),
        leading: Text(''),
        actions: [
          Container(
            margin: EdgeInsets.only(left: 20 * ws),
            child: FlatButton(
              onPressed: () async {

                  showLoaderDialog(context);
                  await _capturePng().then((Uint8List image) async {
                    if(image != null){
                      getInventoryImage.update(base64.encode(image));
                      Navigator.of(context, rootNavigator: true).pop();
                    }else{
                      Navigator.of(context, rootNavigator: true).pop();
                      await showErrorDialog(context: context,errorDesc: 'Có lỗi xảy ra');
                    }
                  });

                  Navigator.pop(context);

              },
              child: Text('Xong',
                  style: TextStyle(color: Colors.white, fontSize: 16)),
            ),
          ),
        ],
      ),
      body: Container(
        margin: EdgeInsets.only(bottom: CUPERTINO_TAB_BAR_HEIGHT),
        child: RepaintBoundary(
          key: _globalKey,
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(color: Colors.black),
            child: Center(
              child: Stack(
                children: [
                  Image.memory(base64.decode(widget.image)),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Địa điểm :  ',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 14 * fs),
                            ),
                            Flexible(
                              child: Text(
                                '${widget.address}',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14 * fs),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          '$formattedDateTime',
                          style:
                              TextStyle(color: Colors.white, fontSize: 14 * fs),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<Uint8List> _capturePng() async {
    try {
      print('inside');
      RenderRepaintBoundary boundary =
          _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      var bs64 = base64Encode(pngBytes);
      print(pngBytes);
      print(bs64);
      setState(() {});
      return pngBytes;
    } catch (e) {
      print(e);
      return null;
    }
  }
}

Future<Position> _determinePosition() async {
  bool serviceEnabled;
  LocationPermission permission;

  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  // serviceEnabled = await Geolocator.LocationServiceDisabledException ();
  // if (!serviceEnabled) {
  //   return Future.error('Location services are disabled.');
  // }

  print(serviceEnabled);

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.deniedForever) {
    return Future.error(
        'Location permissions are permantly denied, we cannot request permissions.');
  }

  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission != LocationPermission.whileInUse &&
        permission != LocationPermission.always) {
      return Future.error(
          'Location permissions are denied (actual value: $permission).');
    }
  }

  return await Geolocator.getCurrentPosition();
}

Future<String> getImage() async {
  String myImage;
  final picker = ImagePicker();
  try {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    print('set image');
    if (pickedFile != null) {
      print('pickedFIle != null');
      // setState(() {
        print('setMyImage');
        myImage = base64.encode(File(pickedFile.path).readAsBytesSync());
        print('Done setMyImage');
        return myImage;
      // });

      // getCurrentAddress();
    } else {
      // Navigator.pop(context);
      return '';
    }
  } catch (e) {
    print(e.toString());
    return '';
  }
}

Future<String> getCurrentAddress() async {
  Address currentAddress;
  await _determinePosition().then((position) async {
    if (position != null) {
      print('======${position.latitude}, ${position.longitude}========');
      // List<Placemark> placemarks = await placemarkFromCoordinates(20.190744, 106.228931);
      // List<Placemark> placemarks = await placemarkFromCoordinates(21.014564, 105.791988);
      // print(placemarks.length);
      // print(placemarks[0].toString());

      final coordinates =
      new Coordinates(position.latitude, position.longitude);
      // final coordinates1 = new Coordinates(21.092132, 105.547936);
      List<Address> addresses =
      await Geocoder.local.findAddressesFromCoordinates(coordinates);
      if (addresses.length > 0) {
        // setState(() {
          currentAddress = addresses.first;

          print(currentAddress.addressLine);
          print(currentAddress.countryName);
          print(currentAddress.countryCode);
          print(currentAddress.featureName);
          print(currentAddress.postalCode);
          print(currentAddress.locality);
          print(currentAddress.subLocality);
          print(currentAddress.adminArea);
          print(currentAddress.subAdminArea);
          print(currentAddress.thoroughfare);
          print(currentAddress.subThoroughfare);
        // });
      }
    }
  });

  String address;
  currentAddress == null
      ? address = 'Chưa có vị trí'
      : address = currentAddress.addressLine.replaceAll('Unnamed Road,', '');
  return address;
}

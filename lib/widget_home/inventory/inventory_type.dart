import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_data_update_model.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_fixed_model.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_result_API_model.dart';
import 'package:qlts/data_sources/data_source_inventory/offline/inventory_offline_model.dart';
import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_image_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:provider/provider.dart';
import 'package:qlts/provider/home_search/search_data_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/repository/repository_database.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_dialog/dialog_notification.dart';
import 'package:qlts/widget_dialog/dialog_success.dart';
import 'package:qlts/widget_home/button_widget.dart';

class InventoryType extends StatefulWidget {
  final token;

  InventoryType({this.token});

  @override
  State<StatefulWidget> createState() => InventoryTypeState();
}

class InventoryTypeState extends State<InventoryType> {
  int currentSelected;

  int _selectConnection;

  List<String> _ketNoi = ['Online', 'Offline'];

  Repository repository = Repository();
  RepositoryDatabase repositoryDatabase =
      RepositoryDatabase(DatabaseProvider.getInstance);

  InventoryResultAPIModel inventoryResultAPIModel;

  var getInventoryData;
  var getInventoryFixedDataProvider;
  var getInventoryConfigData;
  var getInventoryImageProvider;
  var getInventoryDetailData;
  var getTabProvider;
  var getQRCodeProvider;
  var getSearchDataModel;
  var getHomeDataInputProvider;

  @override
  Widget build(BuildContext context) {
    getInventoryData = Provider.of<InventoryData>(context);
    getInventoryFixedDataProvider =
        Provider.of<InventoryFixedDataProvider>(context);
    getInventoryConfigData =
        Provider.of<InventoryConfigData>(context, listen: false);
    getInventoryImageProvider =
        Provider.of<InventoryImageProvider>(context, listen: false);
    getInventoryDetailData =
        Provider.of<InventoryDetailData>(context, listen: false);

    getTabProvider = Provider.of<TabProvider>(context, listen: false);
    getQRCodeProvider = Provider.of<QRCodeProvider>(context, listen: false);
    getSearchDataModel = Provider.of<SearchDataModel>(context, listen: false);

    getHomeDataInputProvider = Provider.of<HomeDataInputProvider>(context);

    return Column(
      children: [
        Card(
          margin: EdgeInsets.only(top: 15 * ws, left: 15 * ws, right: 15 * ws),
          shadowColor: Colors.white70,
          elevation: 10.0 * ws,
          color: Colors.white.withOpacity(1),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          child: Container(
            padding: EdgeInsets.only(
                top: 20 * ws, bottom: 10 * ws, left: 20 * ws, right: 20 * ws),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ///Chọn đợt kiểm kê tài sản
                _buildChonDotKiemKe(),
                Divider(color: Colors.black),

                ///Chọn sổ
                _buildChonSo(),

                ///Kết nối: online/offline
                _buildOnlineOrOffline(),

                ///Upload và tải online
                _buildUploadData()
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildChonDotKiemKe() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Text(
              CHON_DOT,
              style: TextStyle(fontSize: 13 * fs),
            ),
          ],
        ),
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Container(
                      margin: EdgeInsets.only(right: 20 * ws),
                      child: Text(
                          getInventoryData.list.length == 0
                              ? LACK_OF_INVENTORY
                              : getInventoryData
                                  .list[getInventoryData.currentIndex].ten,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Color(0xFF050505),
                              fontWeight: FontWeight.bold,
                              fontSize: 16 * fs)))),
              IconButton(
                onPressed: () async {
                  /// Kiểm tra
                  if (getInventoryData.list.length > 0) {
                    currentSelected = getInventoryData.currentIndex;

                    await showModalBottomSheet(
                        context: context,
                        backgroundColor: Colors.blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              topRight: Radius.circular(15)),
                        ),
                        elevation: 10 * ws,
                        builder: (BuildContext context) {
                          return StatefulBuilder(builder:
                              (BuildContext context, StateSetter state) {
                            return Container(
                              child: Column(
                                // mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 50 * ws,
                                    child: Center(
                                      child: Text(
                                        LIST_INVENTORY,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20 * fs),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: 20 * ws, right: 20 * ws),
                                      decoration:
                                          BoxDecoration(color: Colors.white),
                                      // child: SizedBox(
                                      //   height: 220*ws,
                                      child: ListView.builder(
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return RadioListTile(
                                              activeColor: Colors.blue,
                                              value: index,
                                              groupValue: currentSelected,
                                              onChanged: (value) {
                                                state(() {
                                                  currentSelected = value;
                                                  print(currentSelected);
                                                });
                                              },
                                              title: Text(
                                                  '${getInventoryData.list[index].ten}'),
                                            );
                                          },
                                          itemCount:
                                              getInventoryData.list.length),
                                    ),
                                  ),
                                  Container(
                                      // height: 50*ws,
                                      width: double.infinity,
                                      padding: EdgeInsets.only(top: 12 * ws),
                                      decoration:
                                          BoxDecoration(color: Colors.white),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          RaisedGradientButton(
                                            marginBottom: 20 * ws +
                                                CUPERTINO_TAB_BAR_HEIGHT,
                                            child: Text(CHON,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: 14 * fs)),
                                            onPressed: () async {
                                              if (currentSelected !=
                                                  getInventoryData
                                                      .currentIndex) {
                                                ///clear database offline
                                                // if (!getInventoryData
                                                //     .emptySqflite) {
                                                //   await repositoryDatabase
                                                //       .databaseRepositoryOffline
                                                //       .clear();
                                                //   getInventoryData
                                                //       .updateEmptySqflite(
                                                //           true);
                                                // }

                                                ///update index, id
                                                getInventoryData.currentIndex =
                                                    currentSelected;

                                                ///Sau khi chọn đợt kiểm kê => sẽ gọi API danh sách chốt sổ
                                                showLoaderDialog(context);

                                                ///Gọi API danh sách chốt sổ
                                                InventoryFixedModel
                                                    inventoryFixedModel =
                                                    await repository
                                                        .fetchGetInventoryFixed(
                                                            token: widget.token,
                                                            inventoryId: getInventoryData
                                                                .list[getInventoryData
                                                                    .currentIndex]
                                                                .id
                                                                .toString());

                                                if (inventoryFixedModel
                                                        .errorCode !=
                                                    SUCCESS) {
                                                  if (inventoryFixedModel
                                                          .errorCode !=
                                                      SUCCESS) {
                                                    if (inventoryFixedModel
                                                            .errorCode ==
                                                        TIME_OUT) {
                                                      getInventoryData.clear();
                                                      getInventoryFixedDataProvider
                                                          .clear();
                                                      getInventoryConfigData
                                                          .clear();
                                                      getInventoryDetailData
                                                          .clear();
                                                      getInventoryImageProvider
                                                          .clear();
                                                      getHomeDataInputProvider
                                                          .clear();
                                                      getQRCodeProvider.clear();
                                                      getTabProvider.clear();
                                                      getSearchDataModel
                                                          .clear();
                                                    }

                                                    Navigator.of(context,
                                                            rootNavigator: true)
                                                        .pop();

                                                    ///Show error code
                                                    showErrorDialog(
                                                        context: context,
                                                        errorCode:
                                                            inventoryFixedModel
                                                                .errorCode,
                                                        errorDesc:
                                                            inventoryFixedModel
                                                                .errorDesc);
                                                  }
                                                } else {
                                                  ///list có phần tử mới update
                                                  getInventoryFixedDataProvider
                                                      .update(inventoryFixedModel
                                                          .listInventoryFixed);
                                                  getInventoryFixedDataProvider
                                                      .updateId(0);

                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop();
                                                }
                                                Navigator.pop(context);
                                              } else {
                                                Navigator.pop(context);
                                              }
                                            },
                                          ),
                                        ],
                                      ))
                                ],
                              ),
                            );
                          });
                        });
                  }
                },
                icon: Icon(Icons.expand_more_rounded),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildChonSo() {
    return Visibility(
      visible: !getInventoryData.online,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 10 * ws, top: 15 * ws),
                child: Text(
                  CHON_SO,
                  style: TextStyle(fontSize: 13 * fs),
                ),
              ),
            ],
          ),
          Container(
            child: Row(
              children: [
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20 * ws),
                        child: Text(
                            getInventoryFixedDataProvider
                                        .listInventoryFixed.length ==
                                    0
                                ? EMPTY_FIXED_INVENTORY
                                : getInventoryFixedDataProvider
                                    .listInventoryFixed[
                                        getInventoryFixedDataProvider
                                            .currentIndex]
                                    .ten,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Color(0xFF050505),
                                fontWeight: FontWeight.bold,
                                fontSize: 16 * fs)))),
                IconButton(
                  onPressed: () async {
                    if (getInventoryFixedDataProvider
                            .listInventoryFixed.length >
                        0) {
                      print(getInventoryFixedDataProvider
                          .listInventoryFixed.length);

                      currentSelected =
                          getInventoryFixedDataProvider.currentIndex;

                      await showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15)),
                          ),
                          elevation: 10 * ws,
                          builder: (BuildContext context) {
                            return StatefulBuilder(builder:
                                (BuildContext context, StateSetter state) {
                              return Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          bottom: 20 * ws, top: 20 * ws),
                                      child: Center(
                                        child: Text(
                                          DANH_SACH_CHOT_SO,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20 * fs),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 20 * ws, right: 20 * ws),
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: ListView.builder(
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return RadioListTile(
                                                activeColor: Colors.blue,
                                                value: index,
                                                groupValue: currentSelected,
                                                onChanged: (value) {
                                                  setState(() {
                                                    currentSelected = value;
                                                  });
                                                },
                                                title: Text(
                                                    '${getInventoryFixedDataProvider.listInventoryFixed[index].ten}'),
                                              );
                                            },
                                            itemCount:
                                                getInventoryFixedDataProvider
                                                    .listInventoryFixed.length),
                                      ),
                                    ),
                                    Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.only(top: 12 * ws),
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                            RaisedGradientButton(
                                              marginBottom: 20 * ws +
                                                  CUPERTINO_TAB_BAR_HEIGHT,
                                              child: Text(CHON,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      fontSize: 14 * fs)),
                                              onPressed: () async {
                                                if (currentSelected !=
                                                    getInventoryFixedDataProvider
                                                        .currentIndex) {
                                                  ///clear database offline
                                                  // if (!getInventoryData
                                                  //     .emptySqflite) {
                                                  //   await repositoryDatabase
                                                  //       .databaseRepositoryOffline
                                                  //       .clear();
                                                  //   getInventoryData
                                                  //       .updateEmptySqflite(
                                                  //           true);
                                                  // }
                                                  getInventoryFixedDataProvider
                                                      .updateId(
                                                          currentSelected);
                                                }
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              );
                            });
                          });
                    }
                  },
                  icon: Icon(Icons.expand_more_rounded),
                )
              ],
            ),
          ),
          Divider(color: Colors.black),
        ],
      ),
    );
  }

  Widget _buildOnlineOrOffline() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10 * ws),
              child: Text(
                KET_NOI,
                style: TextStyle(fontSize: 13 * fs),
              ),
            ),
          ],
        ),
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Container(
                      margin: EdgeInsets.only(right: 20 * ws),
                      child: Text(
                          getInventoryData.online ? 'Online' : 'Offline',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Color(0xFF050505),
                              fontWeight: FontWeight.bold,
                              fontSize: 16 * fs)))),
              IconButton(
                onPressed: () async {
                  int _isOnline = getInventoryData.online ? 0 : 1;
                  _selectConnection = _isOnline;

                  await showModalBottomSheet(
                      context: context,
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15)),
                      ),
                      elevation: 10 * ws,
                      builder: (BuildContext context) {
                        return StatefulBuilder(
                            builder: (BuildContext context, StateSetter state) {
                          return Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: 50 * ws,
                                  child: Center(
                                    child: Text(
                                      KET_NOI_TITLE,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20 * fs),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left: 20 * ws, right: 20 * ws),
                                    decoration:
                                        BoxDecoration(color: Colors.white),
                                    // child: SizedBox(
                                    //   height: 220*ws,
                                    child: ListView.builder(
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return RadioListTile(
                                            activeColor: Colors.blue,
                                            value: index,
                                            groupValue: _selectConnection,
                                            onChanged: (value) {
                                              state(() {
                                                _selectConnection = value;
                                                print(_selectConnection);
                                              });
                                            },
                                            title: Text('${_ketNoi[index]}'),
                                          );
                                        },
                                        itemCount: _ketNoi.length),
                                  ),
                                ),
                                Container(
                                    // height: 50*ws,
                                    width: double.infinity,
                                    padding: EdgeInsets.only(top: 12 * ws),
                                    decoration:
                                        BoxDecoration(color: Colors.white),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        RaisedGradientButton(
                                          marginBottom: 20 * ws +
                                              CUPERTINO_TAB_BAR_HEIGHT,
                                          child: Text(CHON,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 14 * fs)),
                                          onPressed: () async {
                                            if (_selectConnection !=
                                                _isOnline) {
                                              getInventoryData
                                                  .updateOnlineState(
                                                      _selectConnection == 0
                                                          ? true
                                                          : false);
                                              getHomeDataInputProvider.clear();
                                              getInventoryDetailData.clear();
                                              getSearchDataModel.clear();
                                            }
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    ))
                              ],
                            ),
                          );
                        });
                      });
                },
                icon: Icon(Icons.expand_more_rounded),
              )
            ],
          ),
        ),
        Visibility(
            visible: !getInventoryData.online,
            child: Divider(color: Colors.black)),
      ],
    );
  }

  Widget _buildUploadData() {
    return Visibility(
      visible: !getInventoryData.online,
      child: Container(
        margin: EdgeInsets.only(top: 20 * ws),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RaisedGradientButton(
              child: Text(
                DOWLOAD,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * fs),
              ),

              ///Goi API lay du lieu luu vao sqlite
              onPressed: () async {
                ///call API get data offline
                if (getInventoryData.currentId == -1) {
                  showErrorDialog(
                      context: context, errorDesc: LACK_OF_INVENTORY);
                } else if (getInventoryFixedDataProvider.currentId == -1) {
                  showErrorDialog(
                      context: context, errorDesc: LACK_OF_FIXED_INVENTORY);
                } else {
                  showLoaderDialog(context);
                  InventoryOfflineModel inventoryOfflineModel =
                      await repository.fetchGetDataOfflineInventory(
                          token: widget.token,
                          inventoryId: getInventoryData.currentId.toString(),
                          fixedId: getInventoryFixedDataProvider.currentId
                              .toString());
                  if (inventoryOfflineModel.errorCode != SUCCESS) {
                    if (inventoryOfflineModel.errorCode == TIME_OUT) {
                      getInventoryData.clear();
                      getInventoryFixedDataProvider.clear();
                      getInventoryConfigData.clear();
                      getInventoryDetailData.clear();
                      getInventoryImageProvider.clear();
                      getHomeDataInputProvider.clear();
                      getQRCodeProvider.clear();
                      getTabProvider.clear();
                      getSearchDataModel.clear();
                    }

                    // Navigator.pop(context);
                    Navigator.of(context, rootNavigator: true).pop();
                    showErrorDialog(
                        context: context,
                        errorCode: inventoryOfflineModel.errorCode,
                        errorDesc: inventoryOfflineModel.errorDesc);
                  } else {
                    ///Thanh cong => check list.length > 0 => update Sqlite
                    if (inventoryOfflineModel.listInventoryOffline.length > 0) {
                      ///clear csdl
                      repositoryDatabase.databaseRepositoryOffline.clear();

                      ///luu vao sqflite
                      print(
                          '**********length*****${inventoryOfflineModel.listInventoryOffline.length}');
                      // for (int i = 0; i < inventoryOfflineModel.listInventoryOffline.length; i++) {
                      // int currentRow = await repositoryDatabase.databaseRepositoryOffline.insertData(inventoryOfflineModel.listInventoryOffline[i]);
                      // print(
                      //     '----insert sqlite-----$i: $currentRow---------');
                      // }
                      List<dynamic> res = await repositoryDatabase
                          .databaseRepositoryOffline
                          .insertBatch(
                              inventoryOfflineModel.listInventoryOffline);

                      ///kiem insert that bai
                      int count = await repositoryDatabase
                          .databaseRepositoryOffline
                          .countRow();
                      print('======count = $count============');
                      if (inventoryOfflineModel.listInventoryOffline.length !=
                              count ||
                          res == null) {
                        // Navigator.pop(context);
                        Navigator.of(context, rootNavigator: true).pop();
                        repositoryDatabase.databaseRepositoryOffline.clear();

                        showErrorDialog(
                            context: context, errorDesc: ERROR_DOWLOAD);
                      } else {
                        getInventoryData.updateEmptySqflite(false);
                        // Navigator.pop(context);
                        Navigator.of(context, rootNavigator: true).pop();
                        showSuccessDialog(
                            context: context, message: SUCCESS_DOWLOAD);
                      }
                    } else {
                      print(
                          'inventoryOfflineModel.listInventoryOffline.length < 0');
                      // Navigator.pop(context);
                      Navigator.of(context, rootNavigator: true).pop();
                      showErrorDialog(
                          context: context, errorDesc: EMPTY_SQFLITE);
                    }
                  }
                }
              },
            ),
            RaisedGradientButton(
              child: Text(
                UPLOAD,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * fs),
              ),
              onPressed: () async {
                int count = await repositoryDatabase.databaseRepositoryOffline
                    .countRow();
                int countRowUpdated = await repositoryDatabase
                    .databaseRepositoryOffline
                    .countRowUpdated();
                if (count <= 0) {
                  showErrorDialog(context: context, errorDesc: EMPTY_SQFLITE);
                } else if (countRowUpdated <= 0) {
                  showErrorDialog(context: context, errorDesc: EMPTY_UPDATED);
                } else if (getInventoryFixedDataProvider.currentIndex == -1) {
                  showErrorDialog(
                      context: context, errorDesc: LACK_OF_FIXED_INVENTORY);
                } else {
                  MyDialog.showDialog(context,
                      msg: 'Tổng số dữ liệu: $countRowUpdated',
                      cancelTitle: 'Đóng',
                      okTitle: 'Xác nhận', okHandler: () async {
                    Navigator.of(context, rootNavigator: true).pop();
                    showLoaderDialog(context);
                    Map<String, dynamic> res =
                        await repositoryDatabase.databaseRepositoryOffline
                            // .getDataUpload();
                            .getData();
                    print('--------data upload-----------$res');

                    ///Upload data
                    if (res == null || res == {}) {
                      Navigator.of(context, rootNavigator: true).pop();
                      showErrorDialog(
                          context: this.context,
                          errorDesc: LACK_OF_INFORMATION_UPDATE);
                    } else {
                      InventoryDataUpload inventoryDataUpload =
                          InventoryDataUpload.getInstance(
                              token: widget.token,
                              inventoryId:
                                  getInventoryData.currentId.toString(),
                              assetId: getInventoryDetailData.idChiTietQLTS
                                  .toString(),
                              statusCode: getInventoryConfigData
                                          .listDataStatusConfig.length >
                                      0
                                  ? getInventoryConfigData
                                      .listDataStatusConfig[
                                          getInventoryConfigData.currentStatus]
                                      .statusCode
                                      .toString()
                                  : '',
                              dataUpload: res,
                              scenePhoto:
                                  'getInventoryImageProvider.imageBase64',
                              description: 'description');

                      inventoryResultAPIModel =
                          await repository.fetchUploadData(
                              inventoryDataUpload: inventoryDataUpload);

                      if (inventoryResultAPIModel.errorCode == SUCCESS) {
                        ///clear du lieu sau khi upload thanh cong
                        await repositoryDatabase.databaseRepositoryOffline
                            .clear();

                        Navigator.of(context, rootNavigator: true).pop();
                        showSuccessDialog(
                            context: this.context, message: SUCCESS_UPLOAD);
                      } else {
                        Navigator.of(context, rootNavigator: true).pop();

                        if (inventoryResultAPIModel.errorCode == TIME_OUT) {
                          getInventoryData.clear();
                          getInventoryFixedDataProvider.clear();
                          getInventoryConfigData.clear();
                          getInventoryDetailData.clear();
                          getInventoryImageProvider.clear();
                          getHomeDataInputProvider.clear();
                          getQRCodeProvider.clear();
                          getTabProvider.clear();
                          getSearchDataModel.clear();
                        }

                        showErrorDialog(
                            context: this.context,
                            errorCode: inventoryResultAPIModel.errorCode,
                            errorDesc: inventoryResultAPIModel.errorDesc);
                      }
                    }
                  });
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

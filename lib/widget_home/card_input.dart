import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/home_search/search_data_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/widget_home/button_widget.dart';

class CardInput extends StatefulWidget {
  final isSearch;
  final Function onSearchPress;
  final Function onCapturePress;

  CardInput(
      {@required this.isSearch,
      @required this.onSearchPress,
      this.onCapturePress});

  @override
  State<StatefulWidget> createState() => CardInputState();
}

class CardInputState extends State<CardInput> {
  var getSearchDataModel;
  var getQRCodeProvider;
  var getInventoryData;
  var getInventoryDetailData;
  var getHomeDataInputProvider;

  @override
  Widget build(BuildContext context) {
    getSearchDataModel = Provider.of<SearchDataModel>(context);
    getQRCodeProvider = Provider.of<QRCodeProvider>(context);
    getInventoryData = Provider.of<InventoryData>(context, listen: false);
    getInventoryDetailData = Provider.of<InventoryDetailData>(context);
    getHomeDataInputProvider = Provider.of<HomeDataInputProvider>(context);

    return Card(
      margin: EdgeInsets.only(
          top: 15 * ws, bottom: 10 * ws, right: 15 * ws, left: 15 * ws),
      shadowColor: Colors.white70,
      elevation: 10.0 * ws,
      color: Colors.white.withOpacity(1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      child: Container(
        height: 310 * ws,
        padding: EdgeInsets.only(
            bottom: 10 * ws, top: 15 * ws, right: 20 * ws, left: 20 * ws),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildSoThe(),
            _buildSerial(),
            _buildQrCode(),
            ///Click Tra cuu
            _buildTraCuu(),
            _buildErrorMess()
          ],
        ),
      ),
    );
  }

  Widget _buildSoThe() {
    return Expanded(
      flex: 1,
      child: Row(
        children: [
          ///So the
          Expanded(
              flex: 10,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        text: TextSpan(
                            style: TextStyle(fontSize: 13 * fs),
                            children: [
                          TextSpan(
                              text: SO_THE,
                              style: TextStyle(
                                  color: (getHomeDataInputProvider.type == 1 ||
                                          getHomeDataInputProvider.type == -1)
                                      ? Color(0xFF050505)
                                      : Colors.grey)),
                          TextSpan(
                              text: ' (*)',
                              style: TextStyle(
                                  color: getHomeDataInputProvider.type == 1
                                      ? Colors.red
                                      : Colors.transparent))
                        ])),
                    TextField(
                      enabled: (getHomeDataInputProvider.type == 1 ||
                          getHomeDataInputProvider.type == -1),
                      onChanged: (data) {
                        ///Clear danh sach cu khi input thay doi
                        if (!getQRCodeProvider.isEmpty())
                          getQRCodeProvider.clear();
                        if (getInventoryData.online) {
                          if (widget.isSearch &&
                              getSearchDataModel.listSearchResults.length > 0)
                            getSearchDataModel.clear();
                          else if (!widget.isSearch &&
                              getInventoryDetailData
                                      .listInventoryDetail.length >
                                  0) getInventoryDetailData.clear();
                        } else {
                          if (getInventoryDetailData.inventoryOffline != null)
                            getInventoryDetailData.clear();
                        }

                        getHomeDataInputProvider.updateSoThe(data);
                        getHomeDataInputProvider.updateError(false);

                        if (data != '') {
                          if (getHomeDataInputProvider.type != 1)
                            getHomeDataInputProvider.updateType(1);
                          print('type: ${getHomeDataInputProvider.type}');
                        } else {
                          if (getHomeDataInputProvider.empty())
                            getHomeDataInputProvider.updateType(-1);
                        }
                      },
                      controller: getHomeDataInputProvider.soTheController,
                      style: TextStyle(
                        fontSize: 16 * fs,
                        color: Color(0xFF050505),
                        fontWeight: FontWeight.bold,
                      ),
                      decoration: InputDecoration(
                          hintText: HINT_SO_THE,
                          hintStyle: TextStyle(color: Color(0xFF9E9E9E))),
                    ),
                  ],
                ),
              )),
          Spacer(flex: 1),

          /// Id
          Expanded(
              flex: 10,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        text: TextSpan(
                            style: TextStyle(
                                color: Color(0xFF050505), fontSize: 13 * fs),
                            children: [
                          TextSpan(
                              text: ID_CHI_TIET,
                              style: TextStyle(
                                  color: (getHomeDataInputProvider.type == 1 ||
                                          getHomeDataInputProvider.type == -1)
                                      ? Color(0xFF050505)
                                      : Colors.grey)),
                          TextSpan(
                              text: ' (*)',
                              style: TextStyle(
                                  color: getHomeDataInputProvider.type == 1
                                      ? Colors.red
                                      : Colors.transparent))
                        ])),
                    TextField(
                      enabled: (getHomeDataInputProvider.type == 1 ||
                          getHomeDataInputProvider.type == -1),
                      onChanged: (data) {
                        ///Clear danh sach cu khi input thay doi
                        if (!getQRCodeProvider.isEmpty())
                          getQRCodeProvider.clear();
                        if (getInventoryData.online) {
                          if (widget.isSearch &&
                              getSearchDataModel.listSearchResults.length > 0)
                            getSearchDataModel.clear();
                          else if (!widget.isSearch &&
                              getInventoryDetailData
                                      .listInventoryDetail.length >
                                  0) getInventoryDetailData.clear();
                        } else {
                          if (getInventoryDetailData.inventoryOffline != null)
                            getInventoryDetailData.clear();
                        }

                        getHomeDataInputProvider.updateId(data);
                        getHomeDataInputProvider.updateError(false);
                        if (data != '') {
                          if (getHomeDataInputProvider.type != 1)
                            getHomeDataInputProvider.updateType(1);
                        } else {
                          if (getHomeDataInputProvider.empty())
                            getHomeDataInputProvider.updateType(-1);
                        }
                      },
                      controller: getHomeDataInputProvider.serialController,
                      style: TextStyle(
                        fontSize: 16 * fs,
                        color: Color(0xFF050505),
                        fontWeight: FontWeight.bold,
                      ),
                      decoration: InputDecoration(
                          hintText: HINT_ID_CHI_TIET,
                          hintStyle: TextStyle(color: Color(0xFF9E9E9E))),
                    ),
                  ],
                ),
              )),
        ],
      ),
    );
  }

  Widget _buildSerial() {
    return Expanded(
      flex: 1,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              // margin: EdgeInsets.only(top: 5 * ws),
              child: RichText(
                  text: TextSpan(
                      style: TextStyle(
                          color: Color(0xFF050505), fontSize: 13 * fs),
                      children: [
                    TextSpan(
                        text: SERIAL,
                        style: TextStyle(
                            color: (getHomeDataInputProvider.type == 2 ||
                                    getHomeDataInputProvider.type == -1)
                                ? Color(0xFF050505)
                                : Colors.grey)),
                    TextSpan(
                        text: ' (*)',
                        style: TextStyle(
                            color: getHomeDataInputProvider.type == 2
                                ? Colors.red
                                : Colors.transparent))
                  ])),
            ),
            TextField(
              enabled: (getHomeDataInputProvider.type == 2 ||
                  getHomeDataInputProvider.type == -1),
              onChanged: (data) {
                ///Clear danh sach cu khi input thay doi
                if (!getQRCodeProvider.isEmpty()) getQRCodeProvider.clear();
                if (getInventoryData.online) {
                  if (widget.isSearch &&
                      getSearchDataModel.listSearchResults.length > 0)
                    getSearchDataModel.clear();
                  else if (!widget.isSearch &&
                      getInventoryDetailData.listInventoryDetail.length > 0)
                    getInventoryDetailData.clear();
                } else {
                  if (getInventoryDetailData.inventoryOffline != null)
                    getInventoryDetailData.clear();
                }
                getHomeDataInputProvider.updateSerial(data);
                getHomeDataInputProvider.updateError(false);
                if (data != '') {
                  if (getHomeDataInputProvider.type != 2)
                    getHomeDataInputProvider.updateType(2);
                } else {
                  if (getHomeDataInputProvider.empty())
                    getHomeDataInputProvider.updateType(-1);
                }
              },
              controller: getHomeDataInputProvider.idController,
              style: TextStyle(
                fontSize: 16 * fs,
                color: Color(0xFF050505),
                fontWeight: FontWeight.bold,
              ),
              decoration: InputDecoration(
                  hintText: HINT_SERIAL,
                  hintStyle: TextStyle(color: Color(0xFF9E9E9E))),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildQrCode() {
    return Expanded(
        flex: 1,
        child: Container(
          margin: EdgeInsets.only(bottom: 10 * hs),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                // margin: EdgeInsets.only(top: 5*hs),
                child: RichText(
                    text: TextSpan(
                        style: TextStyle(
                            color: Color(0xFF050505), fontSize: 13 * fs),
                        children: [
                      TextSpan(
                          text: QRCODE,
                          style: TextStyle(
                              color: (getHomeDataInputProvider.type == 3 ||
                                      getHomeDataInputProvider.type == -1)
                                  ? Color(0xFF050505)
                                  : Colors.grey)),
                      TextSpan(
                          text: ' (*)',
                          style: TextStyle(
                              color: getHomeDataInputProvider.type == 3
                                  ? Colors.red
                                  : Colors.transparent))
                    ])),
              ),
              TextField(
                enabled: (getHomeDataInputProvider.type == 3 ||
                    getHomeDataInputProvider.type == -1),
                onChanged: (data) {
                  ///Clear danh sach cu khi input thay doi

                  if (!getQRCodeProvider.isEmpty()) getQRCodeProvider.clear();
                  if (getInventoryData.online) {
                    if (widget.isSearch &&
                        getSearchDataModel.listSearchResults.length > 0)
                      getSearchDataModel.clear();
                    else if (!widget.isSearch &&
                        getInventoryDetailData.listInventoryDetail.length > 0)
                      getInventoryDetailData.clear();
                  } else {
                    if (getInventoryDetailData.inventoryOffline != null)
                      getInventoryDetailData.clear();
                  }

                  getHomeDataInputProvider.updateQrCode(data);
                  getHomeDataInputProvider.updateError(false);
                  if (data != '') {
                    if (getHomeDataInputProvider.type != 3)
                      getHomeDataInputProvider.updateType(3);
                  } else {
                    if (getHomeDataInputProvider.empty())
                      getHomeDataInputProvider.updateType(-1);
                  }
                },
                controller: getHomeDataInputProvider.qrcodeController,
                style: TextStyle(
                  fontSize: 16 * fs,
                  color: Color(0xFF050505),
                  fontWeight: FontWeight.bold,
                ),
                decoration: InputDecoration(
                    hintText: HINT_QRCODE,
                    hintStyle: TextStyle(color: Color(0xFF9E9E9E))),
              ),
            ],
          ),
        ));
  }

  Widget _buildTraCuu() {
    return Row(
      mainAxisAlignment: widget.onCapturePress != null
          ? (getInventoryDetailData.listInventoryDetail.length > 0 ||
                  getInventoryDetailData.inventoryOffline != null
              ? MainAxisAlignment.spaceEvenly
              : MainAxisAlignment.center)
          : MainAxisAlignment.center,
      children: [
        Visibility(
          visible: widget.onCapturePress == null
              ? false
              : (getInventoryDetailData.listInventoryDetail.length > 0 ||
                      getInventoryDetailData.inventoryOffline != null
                  ? true
                  : false),
          child: RaisedGradientButton(
              width: 160 * ws,
              onPressed: widget.onCapturePress,
              child: Text(
                CHUP_ANH,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * fs),
              )),
        ),
        RaisedGradientButton(
          onPressed: widget.onSearchPress,
          child: Text(
            TRA_CUU,
            style: TextStyle(
                color: Colors.white,
                fontSize: 14 * fs,
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  Widget _buildErrorMess() {
    return Visibility(
      visible: getHomeDataInputProvider.error,
      child: Container(
        margin: EdgeInsets.only(top: 15 * hs),
        child: Text(
          LACK_OF_INFORMATION_INPUT,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }
}

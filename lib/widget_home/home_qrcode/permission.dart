import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qlts/constant/asset.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/widget_home/home_qrcode/qrcode.dart';

class PermissionWidget extends StatefulWidget {
  final permission;
  final token;
  final isSearch;

  PermissionWidget({Key key, this.token, this.isSearch, this.permission})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => PermissionState(this.permission);
}

class PermissionState extends State<PermissionWidget> {
  final Permission _permission;
  PermissionStatus _permissionStatus = PermissionStatus.undetermined;
  bool _permanentlyDenied = false;

  // Function callback;

  PermissionState(this._permission);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _listenForPermissionStatus();
  }

  void _listenForPermissionStatus() async {
    final status = await _permission.status;
    if (this.mounted) {
      setState(() {
        if (status == PermissionStatus.permanentlyDenied ||
            status == PermissionStatus.restricted) {
          _permanentlyDenied = true;
        }
        _permissionStatus = status;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: (_permissionStatus == PermissionStatus.granted)
            ? Container(
                height: 230 * ws,
                width: 300 * ws,
                child: QRCodeWidget(
                    token: widget.token, isSearch: widget.isSearch))
            : GestureDetector(
                onTap: () {
                  requestPermission(_permission);
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 28,
                      margin: EdgeInsets.only(top: 10 * ws, bottom: 30 * ws),
                      child: Text(
                        ALLOW_CAMERA,
                        style: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF00A2E5),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(bottom: 40 * ws),
                        child: SvgPicture.asset(IMAGE_CAMERA)),
                  ],
                ),
              ),
      ),
    );
  }

  void requestPermission(Permission permission) async {
    final status = await permission.request();

    if ((status == PermissionStatus.restricted ||
            status == PermissionStatus.permanentlyDenied) &&
        _permanentlyDenied) {
      openAppSettings();
    }
    setState(() {
      if (status == PermissionStatus.permanentlyDenied ||
          status == PermissionStatus.restricted) {
        _permanentlyDenied = true;
      }
      _permissionStatus = status;
      print(_permissionStatus);
    });
  }
}

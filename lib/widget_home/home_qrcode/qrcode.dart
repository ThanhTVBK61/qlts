import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_detail_model.dart';
import 'package:qlts/data_sources/data_source_search/item_search_model.dart';
import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_detail_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_image_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/home_search/search_data_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/repository/repository_database.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:vibration/vibration.dart';

class QRCodeWidget extends StatefulWidget {
  final isSearch;
  final token;

  QRCodeWidget({@required this.isSearch, @required this.token});

  @override
  State<StatefulWidget> createState() => _QRCodeWidgetState();
}

class _QRCodeWidgetState extends State<QRCodeWidget> {
  var flashState = flashOff;
  var cameraState = frontCamera;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  RepositoryDatabase repositoryDatabase =
      RepositoryDatabase(DatabaseProvider.getInstance);

  // bool pause = false;
  bool _isFlashOn = false;

  ItemSearchModel itemSearchModel;
  InventoryDetailModel inventoryDetailModel;
  Repository repository = Repository();
  String type = 'qrcode';

  final audioCache = AudioCache();

  @override
  Widget build(BuildContext context) {
    var getInventoryData = Provider.of<InventoryData>(context, listen: false);
    var getInventoryImageProvider =
        Provider.of<InventoryImageProvider>(context, listen: false);
    var getQRCodeProvider = Provider.of<QRCodeProvider>(context);
    var getInventoryDetailData =
        Provider.of<InventoryDetailData>(context, listen: false);
    var getSearchDataModel =
        Provider.of<SearchDataModel>(context, listen: false);

    var getHomeDataInputProvider =
        Provider.of<HomeDataInputProvider>(context, listen: false);

    var getInventoryFixedDataProvider =
        Provider.of<InventoryFixedDataProvider>(context, listen: false);

    ///Get Config
    var getInventoryConfigData =
        Provider.of<InventoryConfigData>(context, listen: false);

    var getTabProvider = Provider.of<TabProvider>(context);

    getQRCodeProvider.isPause
        ? controller?.pauseCamera()
        : controller?.resumeCamera();

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          flex: 10,
          child: QRView(
            key: qrKey,
            onQRViewCreated: (QRViewController controller) {
              this.controller = controller;
              print('---------call qrcode----out-----');
              controller.scannedDataStream.listen((data) async {
                print('---------call qrcode----in-----');
                // print('=======data: $data');
                // controller?.pauseCamera();
                // print('---------call qrcode----in in-----');
                // if (await Vibration.hasVibrator()) {
                //   Vibration.vibrate(duration: 500);
                // }
                if ((getTabProvider.isSearch &&
                        getTabProvider.currentTab == 0) ||
                    (!getTabProvider.isSearch &&
                        getTabProvider.currentTab == 1)) {
                  controller?.pauseCamera();
                  print('---------call qrcode----in in-----');
                  if (await Vibration.hasVibrator()) {
                    Vibration.vibrate(duration: 500);
                  }

                  await audioCache.play('audio/beep.mp3', volume: 5);

                  getQRCodeProvider.updatePause(true);
                  print("*****qrcode: data==>> $data");

                  ///call API

                  if (data == '' || data == null) {
                    ///clear data trong input data widget
                    if (!getHomeDataInputProvider.empty()) {
                      getHomeDataInputProvider.clear();
                    }
                    showErrorDialog(context: context, errorDesc: WRONG_QRCODE);
                  } else {
                    getQRCodeProvider.updateData(data);

                    if (!getHomeDataInputProvider.empty()) {
                      getHomeDataInputProvider.clear();
                    }

                    if (widget.isSearch) {
                      getSearchDataModel.clear();
                      print("*****home_search_qrcode: data==>> $data");
                      showLoaderDialog(context);
                      itemSearchModel = await repository.fetchSearchQRCode(
                          token: widget.token, type: type, value: data);
                      if (itemSearchModel.errorCode != SUCCESS) {
                        if (itemSearchModel.errorCode == TIME_OUT) {
                          getSearchDataModel.clear();
                          getQRCodeProvider.clear();
                          getHomeDataInputProvider.clear();
                          getQRCodeProvider.clear();
                          getTabProvider.clear();
                        }
                        Navigator.of(context, rootNavigator: true).pop();

                        ///Show error code
                        if (itemSearchModel.errorCode == '203') {
                          showErrorDialog(
                              context: this.context,
                              errorDesc: ERROR_NOT_FOUND);
                        } else {
                          showErrorDialog(
                              context: this.context,
                              errorCode: itemSearchModel.errorCode,
                              errorDesc: itemSearchModel.errorDesc);
                        }
                      } else {
                        Navigator.of(context, rootNavigator: true).pop();
                        getSearchDataModel
                            .updateListSearch(this.itemSearchModel.results);
                      }
                    } else {
                      // if (getInventoryData.changeStatusOnline)
                      //   getInventoryData.updateChangeStatus();

                      if (getInventoryDetailData.listInventoryDetail.length !=
                          0) {
                        getInventoryDetailData.clear();
                      }
                      // if(!getHomeDataInputProvider.empty()){
                      //   getHomeDataInputProvider.clear();
                      // }
                      print("*****home_inventory_qrcode: data==>> $data");
                      if (getInventoryData.currentId != -1) {
                        if (getInventoryData.online) {
                          getInventoryImageProvider.clear();
                          inventoryDetailModel =
                              await repository.fetchInventoryDetailQRCode(
                                  token: widget.token,
                                  inventoryId:
                                      getInventoryData.currentId.toString(),
                                  typeCode: type,
                                  value: data);
                          if (inventoryDetailModel.errorCode != SUCCESS) {
                            if (inventoryDetailModel.errorCode == TIME_OUT) {
                              getInventoryData.clear();
                              getInventoryFixedDataProvider.clear();
                              getInventoryConfigData.clear();
                              getInventoryDetailData.clear();
                              getInventoryImageProvider.clear();
                              getHomeDataInputProvider.clear();
                              getQRCodeProvider.clear();
                              getTabProvider.clear();
                            }

                            ///Show error code
                            showErrorDialog(
                                context: this.context,
                                errorCode: inventoryDetailModel.errorCode,
                                errorDesc: inventoryDetailModel.errorDesc);
                          } else {
                            getInventoryDetailData.update(inventoryDetailModel);
                          }
                        } else {
                          if (getInventoryData.emptySqflite) {
                            showErrorDialog(
                                context: context, errorDesc: EMPTY_SQFLITE);
                          } else {
                            dynamic inventoryOffline = await repositoryDatabase
                                .databaseRepositoryOffline
                                .getDataByQRCode(
                                    data,
                                    getInventoryConfigData
                                        .listDataVariantConfig);
                            print(
                                '---------inventoryOffline Qrcode-----------');
                            print(inventoryOffline);
                            print('-----Then--Update-----------');
                            if (inventoryOffline != null) {
                              getInventoryDetailData
                                  .updateInventoryOffline(inventoryOffline);
                              getInventoryImageProvider.update(inventoryOffline.image);
                            } else {
                              ///clear danh sách hiện tại

                              if (getInventoryDetailData.inventoryOffline !=
                                  null) {
                                getInventoryDetailData.clear();
                              }

                              showErrorDialog(
                                  context: context, errorDesc: ERROR_NOT_FOUND);
                            }
                          }
                        }
                      } else {
                        showErrorDialog(
                            context: context, errorDesc: LACK_OF_INVENTORY);
                      }
                    }
                  }
                }
              });
            },
            overlay: QrScannerOverlayShape(
              borderColor: Colors.white,
              borderLength: 20,
              borderWidth: 5,
              cutOutSize: 210,
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.only(left: 10 * ws),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: () {
                      getQRCodeProvider.updatePause(!getQRCodeProvider.isPause);
                      getQRCodeProvider.updateData('');

                      // getQRCodeProvider.isPause
                      //     ? controller?.pauseCamera()
                      //     : controller?.resumeCamera();

                      if (!getQRCodeProvider.isPause) {
                        widget.isSearch
                            ? getSearchDataModel.clear()
                            : getInventoryDetailData.clear();
                      }
                    },
                    child: !getQRCodeProvider.isPause
                        ? Icon(
                            Icons.pause,
                            color: Colors.blue,
                          )
                        : Icon(
                            Icons.play_arrow_rounded,
                            color: Colors.grey,
                          ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        if (_isFlashOn) {
                          _isFlashOn = false;
                          controller?.toggleFlash();
                        } else {
                          _isFlashOn = true;
                          controller?.toggleFlash();
                        }
                      });
                    },
                    child: _isFlashOn
                        ? Icon(
                            Icons.flash_on_rounded,
                            color: Colors.blue,
                          )
                        : Icon(
                            Icons.flash_off_rounded,
                            color: Colors.grey,
                          ),
                  ),
                ],
              ),
            ))
      ],
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:qlts/animating_route_transition.dart';
import 'package:qlts/constant/asset.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_data_config_model.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_fixed_model.dart';
import 'package:qlts/data_sources/data_source_inventory/inventory_model.dart';
import 'package:qlts/provider/home_inventory/inventory_config_data_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_fixed_provider.dart';
import 'package:qlts/provider/home_inventory/inventory_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/widget_BG/widget_background.dart';
import 'package:qlts/widget_account/logout.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_home/inventory/inventory_home.dart';
import 'package:qlts/widget_home/search/home_search.dart';
import 'package:qlts/widget_notification/my_notification_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyHomePage extends StatefulWidget {
  final token;

  MyHomePage({this.token});

  @override
  State<StatefulWidget> createState() => MyHomePageState(token: token);
}

class MyHomePageState extends State<MyHomePage> {

  final token;

  MyHomePageState({this.token});

  final CupertinoTabController _controller = CupertinoTabController();
  int _currentIndex = 0;


  // static TextStyle optionStyle =
  //     TextStyle(fontSize: 30 * fs, fontWeight: FontWeight.bold);

  static final List<Widget> _widgetOptions = [
    const MyHomeContent(),
    const MyNotification(),
    const Logout()
  ];

  static final List<GlobalKey<NavigatorState>> _tabNavKey = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>()
  ];

  final items = [
    BottomNavigationBarItem(
        label: 'Trang chủ',
        icon: Column(children: [
          SvgPicture.asset(IMAGE_HOME, placeholderBuilder: (context) {
            return SizedBox(height: 10);
          }),
          Container(
              margin: const EdgeInsets.only(top: 2),
              child: Opacity(
                  opacity: 0.0, child: SvgPicture.asset(IMAGE_DOT))),
        ]),
        activeIcon: Column(children: [
          SvgPicture.asset(IMAGE_HOME_SELECTED),
          Container(
              margin: const EdgeInsets.only(top: 2),
              child: SvgPicture.asset(IMAGE_DOT)),
        ])),
    BottomNavigationBarItem(
        label: 'Thông báo',
        icon: Column(children: [
          SvgPicture.asset(IMAGE_BELL, placeholderBuilder: (context) {
            return SizedBox(height: 10);
          }),
          Container(
              margin: const EdgeInsets.only(top: 2),
              child: Opacity(
                  opacity: 0.0,
                  child: SvgPicture.asset(
                    IMAGE_DOT,
                  ))),
        ]),
        activeIcon: Column(children: [
          SvgPicture.asset(IMAGE_BELL_SELECTED,
              placeholderBuilder: (context) {
                return SizedBox(height: 10);
              }),
          Container(
              margin: const EdgeInsets.only(top: 2),
              child: SvgPicture.asset(IMAGE_DOT)),
        ])),
    BottomNavigationBarItem(
        label: 'Tài khoản',
        icon: Column(children: [
          SvgPicture.asset(IMAGE_ACCOUNT, placeholderBuilder: (context) {
            return Placeholder(
              color: Colors.red,
            );
          }),
          Container(
              margin: const EdgeInsets.only(top: 2),
              child: Opacity(
                  opacity: 0.0, child: SvgPicture.asset(IMAGE_DOT))),
        ]),
        activeIcon: Column(
          children: [
            SvgPicture.asset(IMAGE_ACCOUNT_SELECTED,
                placeholderBuilder: (context) {
              return SizedBox(height: 10);
            }),
            Container(
                margin: const EdgeInsets.only(top: 2),
                child: SvgPicture.asset(IMAGE_DOT))
          ],
        ))
          // activeIcon: Column(children: [
        //   SvgPicture.asset(IMAGE_ACCOUNT_SELECTED,
        //       placeholderBuilder: (context) {
        //         return SizedBox(height: 10);
        //       }),
          // Container(
          //     margin: const EdgeInsets.only(top: 2),
          //     child: SvgPicture.asset(IMAGE_DOT)),
        // ]))
  ];
  //
  // void _onItemTapped(int index) {
  //   setState(() {
  //     _selectedIndex = index;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    print(
        '-------------TOKEN------------\n${widget.token}\n----------------------------');
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: items,
          onTap: (index) {
            if (index == _currentIndex) {
              _tabNavKey.elementAt(index).currentState.popUntil((r) => r.isFirst);
            }
            _currentIndex = index;
          },
        ),
        controller: _controller,
        tabBuilder: (context, index) {
          return CupertinoTabView(
            navigatorKey: _tabNavKey.elementAt(index),
            builder: (BuildContext context) => _widgetOptions.elementAt(index),
          );
        },
    );

    //   Scaffold(
    //   body:   <Widget>[
    //     MyHomeContent(token: widget.token),
    //     MyNotification(),
    //     Logout(token: widget.token),
    //   ].elementAt(_selectedIndex),
    //   bottomNavigationBar: BottomNavigationBar(
    //     items: [
    //       BottomNavigationBarItem(
    //           label: 'Trang chủ',
    //           icon: Column(children: [
    //             SvgPicture.asset(IMAGE_HOME),
    //             Container(
    //                 margin: const EdgeInsets.only(top: 4),
    //                 child: Opacity(
    //                     opacity: 0.0, child: SvgPicture.asset(IMAGE_DOT))),
    //           ]),
    //           activeIcon: Column(children: [
    //             SvgPicture.asset(IMAGE_HOME_SELECTED),
    //             Container(
    //                 margin: const EdgeInsets.only(top: 4),
    //                 child: SvgPicture.asset(IMAGE_DOT)),
    //           ])),
    //       BottomNavigationBarItem(
    //           label: 'Thông báo',
    //           icon: Column(children: [
    //             SvgPicture.asset(IMAGE_BELL),
    //             Container(
    //                 margin: const EdgeInsets.only(top: 4),
    //                 child: Opacity(
    //                     opacity: 0.0, child: SvgPicture.asset(IMAGE_DOT))),
    //           ]),
    //           activeIcon: Column(children: [
    //             SvgPicture.asset(IMAGE_BELL_SELECTED),
    //             Container(
    //                 margin: const EdgeInsets.only(top: 4),
    //                 child: SvgPicture.asset(IMAGE_DOT)),
    //           ])),
    //       BottomNavigationBarItem(
    //           label: 'Tài khoản',
    //           icon: Column(children: [
    //             SvgPicture.asset(IMAGE_ACCOUNT),
    //             Container(
    //                 margin: const EdgeInsets.only(top: 4),
    //                 child: Opacity(
    //                     opacity: 0.0, child: SvgPicture.asset(IMAGE_DOT))),
    //           ]),
    //           activeIcon: Column(children: [
    //             SvgPicture.asset(IMAGE_ACCOUNT_SELECTED),
    //             Container(
    //                 margin: const EdgeInsets.only(top: 4),
    //                 child: SvgPicture.asset(IMAGE_DOT)),
    //           ]))
    //     ],
    //     currentIndex: _selectedIndex,
    //     selectedItemColor: Color(0xFF1265B6),
    //     selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
    //     onTap: _onItemTapped,
    //   ),
    // );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();

  }
}

class MyHomeContent extends StatefulWidget {

  const MyHomeContent();

  @override
  State<StatefulWidget> createState() => MyHomeContentState();
}

// MyHomeContent: Giao diện trang chủ
class MyHomeContentState extends State<MyHomeContent> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String token = '';

  Future<String> getToken() async {
    SharedPreferences prefs = await _prefs;
    return (prefs.getString('token')) ?? '';
  }

  @override
  Widget build(BuildContext context) {
    Repository repository = Repository();

    var getInventoryData = Provider.of<InventoryData>(context, listen: false);
    var getInventoryConfigData =
        Provider.of<InventoryConfigData>(context, listen: false);
    var getInventoryFixedData =
        Provider.of<InventoryFixedDataProvider>(context, listen: false);
    var getTabProvider = Provider.of<TabProvider>(context, listen: false);

    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 270 * ws,
          // decoration: BoxDecoration(
          //     image: DecorationImage(
          //         image: AssetImage(IMAGE_BACKGROUND), fit: BoxFit.fill)),
          child: backgroundImage(context),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Container(
                padding: EdgeInsets.only(top: 10 * ws),
                child: Text(
                  'Kiểm kê tài sản',
                  style: TextStyle(color: Color(0xFFF2F2F9), fontSize: 20 * fs),
                )),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          body: Container(
            margin: EdgeInsets.only(top: 20 * ws),
            child: ListView(
              children: [
                InkWell(
                  onTap: () async {
                    ///TH: getInventory xong => loi => nguoi dung bam vao search
                    getInventoryData.clear();
                    getInventoryConfigData.clear();
                    getInventoryFixedData.clear();

                    getTabProvider.updateIsSearch(true);

                    if(token == ''){
                      token = await getToken();
                    }

                    Navigator.push(
                        context,
                        RouteTransition(
                            widget: HomeSearch(token: token)));
                  },
                  child: Card(
                    margin: EdgeInsets.only(
                        left: 20 * ws, right: 20 * ws, bottom: 70.w),
                    shadowColor: Colors.grey[50],
                    elevation: 10.0 * ws,
                    color: Colors.white.withOpacity(0.9),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            margin: EdgeInsets.only(
                                left: 23 * ws,
                                right: 31 * ws,
                                top: 20 * ws,
                                bottom: 21 * ws),
                            child: SvgPicture.asset(IMAGE_TITLE_SEARCH)),
                        Text('Tra cứu tài sản',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20 * fs,
                                color: Color(0xFF454545)))
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    getTabProvider.updateIsSearch(false);

                    ///clear het => tranh truong hop goi duoc 1 api thi loi
                    getInventoryData.clear();
                    getInventoryConfigData.clear();
                    getInventoryFixedData.clear();


                    showLoaderDialog(context);

                    if(token == ''){
                      token = await getToken();
                    }

                    ///gọi API danh sách kiểm kê tài sản
                    InventoryModel inventoryModel =
                        await repository.fetchGetInventory(token: token);
                    if (inventoryModel.errorCode != SUCCESS) {
                      if (inventoryModel.errorCode != SUCCESS) {
                        Navigator.of(context, rootNavigator: true).pop();

                        ///Show error code
                        showErrorDialog(
                            context: context,
                            errorCode: inventoryModel.errorCode,
                            errorDesc: inventoryModel.errorDesc);
                      }
                    }
                    else {
                      print('--------update inventory data');

                      /// >=1 đợt kiểm kê => gọi danh sách chốt sổ với id đợt kiểm kê đầu tiên
                      if (inventoryModel.listInventoryRound.length > 0) {
                        getInventoryData
                            .update(inventoryModel.listInventoryRound);
                        getInventoryData.updateIndex(0);

                        ///Gọi API danh sách chốt sổ
                        InventoryFixedModel inventoryFixedModel =
                            await repository.fetchGetInventoryFixed(
                                token: token,
                                inventoryId:
                                    getInventoryData.currentId.toString());

                        if (inventoryFixedModel.errorCode != SUCCESS) {
                          if (inventoryFixedModel.errorCode != SUCCESS) {
                            if (inventoryFixedModel.errorCode == TIME_OUT) {
                              getInventoryData.clear();
                            }

                            Navigator.of(context, rootNavigator: true).pop();

                            ///Show error code
                            showErrorDialog(
                                context: context,
                                errorCode: inventoryFixedModel.errorCode,
                                errorDesc: inventoryModel.errorDesc);
                          }
                        }
                        else {
                          ///list có phần tử mới update
                          if (inventoryFixedModel.listInventoryFixed.length >
                              0) {
                            // context.read<InventoryFixedDataProvider>().update(inventoryFixedModel);
                            // context.read<InventoryFixedDataProvider>().updateId(0);
                            getInventoryFixedData
                                .update(inventoryFixedModel.listInventoryFixed);
                            getInventoryFixedData.updateId(0);
                          }

                          // if (getInventoryConfigData
                          //     .listDataStatusConfig.length ==
                          //     0) {
                          //   ///lay du lieu tu sqflite
                          //   List<DataStatusConfig> listStatus =
                          //   await repositoryDatabase
                          //       .databaseRepositoryStatus
                          //       .getData();
                          // if (listStatus.length == 0) {
                          //   print('status == 0 call API');
                          print('call API Config');

                          InventoryDataConfigModel inventoryDataConfigModel =
                              await repository.fetchGetDataConfigInventory(
                                  token: token);
                          if (inventoryDataConfigModel.errorCode != SUCCESS) {
                            ///Lỗi => hiển thị dialog thông báo lỗi
                            if (inventoryDataConfigModel.errorCode != SUCCESS) {
                              if (inventoryDataConfigModel.errorCode ==
                                  TIME_OUT) {
                                print('-----------clear----');
                                getInventoryData.clear();
                                getInventoryFixedData.clear();
                              }

                              Navigator.of(context, rootNavigator: true).pop();

                              ///Show error code
                              showErrorDialog(
                                  context: context,
                                  errorCode: inventoryDataConfigModel.errorCode,
                                  errorDesc: inventoryModel.errorDesc);
                            }
                          } else {
                            if (inventoryDataConfigModel
                                    .listDataStatusConfig.length >
                                0) {
                              getInventoryConfigData
                                  .update(inventoryDataConfigModel);
                              getInventoryConfigData.initIndexStatus(0);

                              ///luu vao sqflite
                              ///print('Luu status vs variant vao sqlite');
                              // inventoryDataConfigModel
                              //     .listDataStatusConfig
                              //     .forEach((dataStatusConfig) {
                              //   repositoryDatabase
                              //       .databaseRepositoryStatus
                              //       .insert(dataStatusConfig);
                              // });
                              //
                              // inventoryDataConfigModel.listDataVariantConfig
                              //     .forEach((dataVariantConfig) {
                              //   repositoryDatabase
                              //       .databaseRepositoryVariant
                              //       .insert(dataVariantConfig);
                              // });

                              // repositoryDatabase.databaseRepositoryStatus.clear();
                              // repositoryDatabase.databaseRepositoryVariant.clear();

                              // await repositoryDatabase.databaseRepositoryStatus.insertBatch(inventoryDataConfigModel.listDataStatusConfig);
                              // await repositoryDatabase.databaseRepositoryVariant.insertBatch(inventoryDataConfigModel.listDataVariantConfig);

                              Navigator.of(context, rootNavigator: true).pop();
                              Navigator.push(
                                  context,
                                  RouteTransition(
                                      widget:
                                          HomeInventory(token: token)));
                            } else {
                              Navigator.of(context, rootNavigator: true).pop();

                              ///Show error code
                              showErrorDialog(
                                  context: context, errorDesc: FAIL_INVENTORY);
                            }
                          }
                        }
                      }
                      else {
                        InventoryDataConfigModel inventoryDataConfigModel =
                            await repository.fetchGetDataConfigInventory(
                                token: token);
                        if (inventoryDataConfigModel.errorCode != SUCCESS) {
                          ///Lỗi => hiển thị dialog thông báo lỗi
                          if (inventoryDataConfigModel.errorCode != SUCCESS) {
                            if (inventoryDataConfigModel.errorCode ==
                                TIME_OUT) {
                              print('-----------clear----');
                              getInventoryData.clear();
                              getInventoryFixedData.clear();
                            }

                            Navigator.of(context, rootNavigator: true).pop();

                            ///Show error code
                            showErrorDialog(
                                context: context,
                                errorCode: inventoryDataConfigModel.errorCode,
                                errorDesc: inventoryModel.errorDesc);
                          }
                        }
                        else {
                          if (inventoryDataConfigModel
                                  .listDataVariantConfig.length >
                              0) {
                            // context.read<InventoryConfigData>().update(inventoryDataConfigModel);
                            // context.read<InventoryConfigData>().updateIndexStatus(0);

                            getInventoryConfigData
                                .update(inventoryDataConfigModel);
                            getInventoryConfigData.initIndexStatus(0);

                            ///luu vao sqflite

                            Navigator.of(context, rootNavigator: true).pop();
                            Navigator.push(
                                context,
                                RouteTransition(
                                    widget:
                                        HomeInventory(token: token)));
                          } else {
                            Navigator.of(context, rootNavigator: true).pop();

                            ///Show error code
                            showErrorDialog(
                                context: context, errorDesc: FAIL_INVENTORY);
                          }
                        }
                      }
                    }
                  },
                  child: Card(
                    margin: EdgeInsets.only(
                        left: 20 * ws, right: 20 * ws, bottom: 15 * ws),
                    shadowColor: Colors.grey[50],
                    elevation: 10.0 * ws,
                    color: Colors.white.withOpacity(0.9),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            margin: EdgeInsets.only(
                                left: 25 * ws,
                                right: 34 * ws,
                                top: 21 * ws,
                                bottom: 22 * ws),
                            child:
                                SvgPicture.asset(IMAGE_TITLE_INSERT_INVENTORY)),
                        Text(TITLE_INVENTORY,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20 * fs,
                                color: Color(0xFF454545)))
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

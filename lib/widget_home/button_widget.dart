import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qlts/constant/constant.dart';

class RaisedGradientButton extends StatelessWidget {
  final Widget child;

  final Function onPressed;
  final double width;
  final double marginTop;
  final double marginBottom;
  final enable;

  RaisedGradientButton(
      {Key key,
      @required this.child,
      this.onPressed,
        this.enable = true,
      this.width,
      this.marginTop,
      this.marginBottom})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 140 * ws,
      height: 40 * ws,
      margin: EdgeInsets.only(bottom: marginBottom ?? 0, top: marginTop ?? 0),
      child: GestureDetector(
        onTap: enable ? onPressed : null,
        child: Stack(
          children: [
            SvgPicture.asset(
              'assets/images/bg_button.svg',
              fit: BoxFit.fill,
            ),
            Center(
              child: child,
            )
          ],
        ),
      ),
    );
  }
}

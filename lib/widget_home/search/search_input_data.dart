import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_search/item_search_model.dart';
import 'package:qlts/provider/home_data_input_provider.dart';
import 'package:qlts/provider/home_search/search_data_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/provider/tab_provider.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_home/card_input.dart';
import 'package:qlts/widget_home/search/search_qrcode.dart';

class SearchInputData extends StatefulWidget {
  final token;

  SearchInputData({this.token});

  @override
  State<StatefulWidget> createState() => SearchInputDataState();
}

class SearchInputDataState extends State<SearchInputData>
    with AutomaticKeepAliveClientMixin<SearchInputData> {
  ItemSearchModel itemSearchModel;
  Repository repository = Repository();

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var getSearchDataModel = Provider.of<SearchDataModel>(context);
    var getQRCodeProvider = Provider.of<QRCodeProvider>(context, listen: false);
    var getHomeDataInputProvider =
        Provider.of<HomeDataInputProvider>(context, listen: false);
    var getTabProvider = Provider.of<TabProvider>(context, listen: false);

    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CardInput(
              isSearch: true,
              onSearchPress: () async {
                FocusScope.of(context).requestFocus(FocusNode());
                getSearchDataModel.clear();
                if (!getHomeDataInputProvider.validate()) {
                  getHomeDataInputProvider.updateError(true);
                } else {
                  showLoaderDialog(context);
                  if (getHomeDataInputProvider.type == 1) {
                    itemSearchModel = await repository.fetchSearchInput(
                        token: widget.token,
                        soThe: getHomeDataInputProvider.soThe.trim(),
                        assetCodeNumber: getHomeDataInputProvider.id.trim());
                  } else if (getHomeDataInputProvider.type == 2) {
                    itemSearchModel = await repository.fetchSearchInputSerial(
                        token: widget.token,
                        serial: getHomeDataInputProvider.serial.trim());
                  } else {
                    itemSearchModel = await repository.fetchSearchQRCode(
                        token: widget.token,
                        type: 'qrcode',
                        value: getHomeDataInputProvider.qrcode.trim());
                  }
                  if (itemSearchModel.errorCode != SUCCESS) {
                    Navigator.of(context, rootNavigator: true).pop();

                    ///Show error code
                    if (itemSearchModel.errorCode == '203') {
                      showErrorDialog(
                          context: this.context, errorDesc: ERROR_NOT_FOUND);
                    } else {
                      if (itemSearchModel.errorCode == TIME_OUT) {
                        getQRCodeProvider.clear();
                        getSearchDataModel.clear();
                        getHomeDataInputProvider.clear();
                        getTabProvider.clear();
                      }
                      showErrorDialog(
                          context: this.context,
                          errorCode: itemSearchModel.errorCode,
                          errorDesc: itemSearchModel.errorDesc);
                    }
                  } else {
                    // getSearchInputDataModel.update(itemSearchModel);
                    getSearchDataModel
                        .updateListSearch(itemSearchModel.results);
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                }
              }),
          Card(
            margin:
                EdgeInsets.only(bottom: 20 * ws + CUPERTINO_TAB_BAR_HEIGHT, right: 15 * ws, left: 15 * ws),
            shadowColor: Colors.white70,
            elevation: 10.0 * ws,
            color: Colors.white.withOpacity(1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            child: Container(
              height: getSearchDataModel.listSearchResults.length == 0
                  ? 90 * ws
                  : 570 * ws,
              margin: EdgeInsets.only(top: 20 * ws, bottom: 20 * ws),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: EdgeInsets.only(right: 10 * ws),
                      child: Container(
                        margin: EdgeInsets.only(bottom: 20 * ws, left: 20 * ws),
                        child: Text(
                          THONG_TIN_TAI_SAN,
                          style: TextStyle(
                              color: Color(0xFF050505),
                              fontWeight: FontWeight.bold,
                              fontSize: 16 * fs),
                        ),
                      )),
                  Expanded(
                    child: CustomScrollView(
                      slivers: [
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (context, index) => ListTile(
                                    title: informationDetail(
                                        this.context,
                                        getSearchDataModel
                                            .listSearchResults[index],
                                        index),
                                  ),
                              childCount:
                                  getSearchDataModel.listSearchResults.length),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

// Container _informationDetail(BuildContext context, Result result, int index) {
//   return Container(
//     margin: EdgeInsets.only(top: 10 * ws, left: 20 * ws, right: 20 * ws),
//     child: Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Container(
//             margin: EdgeInsets.only(bottom: 10 * ws),
//             child: Text(
//               result.tenThuocTinh,
//               style: TextStyle(color: Color(0xFF050505), fontSize: 13 * fs),
//               overflow: TextOverflow.ellipsis,
//             )),
//         TextField(
//           enabled: false,
//           style: TextStyle(
//             fontSize: 16 * fs,
//             color: Color(0xFF050505),
//             fontWeight: FontWeight.bold,
//           ),
//           decoration: InputDecoration(),
//           keyboardType: result.kieuDuLieu == 'text'
//               ? TextInputType.text
//               : TextInputType.number,
//           controller: TextEditingController()..text = result.giaTri ?? '',
//         ),
//       ],
//     ),
//   );
// }

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_source_search/item_search_model.dart';
import 'package:qlts/provider/home_search/search_data_provider.dart';
import 'package:qlts/provider/qrcode_provider.dart';
import 'package:qlts/repository/repository.dart';
import 'package:qlts/widget_home/home_qrcode/permission.dart';

class SearchQRCode extends StatefulWidget {
  final token;

  SearchQRCode({this.token});

  @override
  State<StatefulWidget> createState() => SearchQRCodeState();
}

class SearchQRCodeState extends State<SearchQRCode>
    with AutomaticKeepAliveClientMixin<SearchQRCode> {
  Repository repository = Repository();
  ItemSearchModel itemSearchModel;
  String type = 'qrcode';

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var getSearchDataModel = Provider.of<SearchDataModel>(context);
    var getQRCodeProvider = Provider.of<QRCodeProvider>(context);

    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ///get du lieu tu camera
          Card(
            margin: EdgeInsets.all(15 * ws),
            shadowColor: Colors.white70,
            elevation: 10.0 * ws,
            color: Colors.white.withOpacity(1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            child: Container(
                child: Container(
                    padding: EdgeInsets.all(10 * ws),
                    child: Column(
                      children: [
                        PermissionWidget(
                            token: widget.token,
                            isSearch: true,
                            permission: Permission.camera),
                        // PermissionWidget(
                        //   permission: Permission.camera,
                        //   callback: (data, clear) async {
                        //     if (!clear) {
                        //       if (data == '' || data == null) {
                        //         ///clear data trong input data widget
                        //         if(!getHomeDataInputProvider.empty()){
                        //           getHomeDataInputProvider.clear();
                        //         }
                        //         showErrorDialog(
                        //             context: context,
                        //             errorDesc: WRONG_QRCODE);
                        //       }
                        //       else {
                        //         if(!getHomeDataInputProvider.empty()){
                        //           getHomeDataInputProvider.clear();
                        //         }
                        //         getSearchDataModel.clearListSearchResult();
                        //         getQRCodeProvider.updateData(data);
                        //         print(
                        //             "*****home_search_qrcode: data==>> $data");
                        //         showLoaderDialog(context);
                        //         itemSearchModel =
                        //             await repository.fetchSearchQRCode(
                        //                 token: widget.token,
                        //                 type: type,
                        //                 value: data);
                        //         if (itemSearchModel.errorCode != SUCCESS) {
                        //           if (itemSearchModel.errorCode == TIME_OUT) {
                        //             getSearchDataModel.clear();
                        //             getQRCodeProvider.clear();
                        //             getHomeDataInputProvider.clear();
                        //           }
                        //           Navigator.pop(context);
                        //
                        //           ///Show error code
                        //           if (itemSearchModel.errorCode == '203') {
                        //             showErrorDialog(
                        //                 context: this.context,
                        //                 errorDesc: ERROR_NOT_FOUND);
                        //           } else {
                        //             showErrorDialog(
                        //                 context: this.context,
                        //                 errorCode: itemSearchModel.errorCode,
                        //                 errorDesc: itemSearchModel.errorDesc);
                        //           }
                        //         } else {
                        //           Navigator.pop(context);
                        //           getSearchDataModel
                        //               .updateListSearch(this.itemSearchModel.results);
                        //         }
                        //       }
                        //     } else {
                        //       getSearchDataModel.clearListSearchResult();
                        //     }
                        //   },
                        // ),
                        Visibility(
                          visible:
                              (getQRCodeProvider.data == '') ? false : true,
                          child: Container(
                              margin: EdgeInsets.only(
                                  top: 10 * ws, left: 15 * ws, right: 5 * ws),
                              child: Text(getQRCodeProvider.data ?? '')),
                        )
                      ],
                    ))),
          ),

          Card(
            margin:
                EdgeInsets.only(bottom: 20 * ws + CUPERTINO_TAB_BAR_HEIGHT, right: 20 * ws, left: 20 * ws),
            shadowColor: Colors.white70,
            elevation: 10.0,
            color: Colors.white.withOpacity(1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            child: Container(
              height: getSearchDataModel.listSearchResults.length == 0
                  ? 90 * ws
                  : 570 * ws,
              margin: EdgeInsets.only(top: 20 * ws, bottom: 20 * ws),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: EdgeInsets.only(right: 10 * ws),
                      child: Container(
                        margin: EdgeInsets.only(bottom: 20 * ws, left: 20 * ws),
                        child: Text(
                          THONG_TIN_TAI_SAN,
                          style: TextStyle(
                              color: Color(0xFF050505),
                              fontWeight: FontWeight.bold,
                              fontSize: 16 * fs),
                        ),
                      )),
                  Expanded(
                    child: CustomScrollView(
                      slivers: [
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (context, index) => ListTile(
                                    title: informationDetail(
                                        this.context,
                                        getSearchDataModel
                                            .listSearchResults[index],
                                        index),
                                  ),
                              childCount:
                                  getSearchDataModel.listSearchResults.length),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

///Sử dụng cho cả tìm kiếm bằng nhập thông tin=> hiển thị thông tin chi tiết đợt kiểm kê
Container informationDetail(BuildContext context, Result result, int index) {
  return Container(
    margin: EdgeInsets.only(top: 10 * ws, left: 20 * ws, right: 20 * ws),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: EdgeInsets.only(bottom: 10 * ws),
            child: Text(
              result.tenThuocTinh,
              style: TextStyle(color: Color(0xFF050505), fontSize: 13 * fs),
              overflow: TextOverflow.ellipsis,
            )),
        TextField(
          enabled: false,
          style: TextStyle(
            fontSize: 16 * fs,
            color: Color(0xFF050505),
            fontWeight: FontWeight.bold,
          ),
          decoration: InputDecoration(),
          keyboardType: result.kieuDuLieu == 'text'
              ? TextInputType.text
              : TextInputType.number,
          controller: TextEditingController()..text = result.giaTri ?? '',
        ),
      ],
    ),
  );
}

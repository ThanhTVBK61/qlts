import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qlts/constant/asset.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/repository/repository_database.dart';
import 'package:qlts/widget_home/button_widget.dart';
import 'package:qlts/widget_login/my_login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> showErrorDialog(
    {@required BuildContext context, String errorCode, String errorDesc}) async {
  String connectionDesc = '';
  RepositoryDatabase repositoryDatabase =
      RepositoryDatabase(DatabaseProvider.getInstance);

  if (errorCode == '-1') {
    bool connected = await checkConnection();
    if (!connected) {
      connectionDesc = 'Vui lòng kiểm tra kết nối mạng';
    }
  }

  Dialog errorDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: Container(
        width: 335 * ws,
        padding: EdgeInsets.only(top: 20 * ws),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [

            SvgPicture.asset(IMAGE_ERROR),
            Container(
                margin: EdgeInsets.only(top: 15 * ws, bottom: 5),
                child: Text(
                  'Thông báo !',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                )),
            Container(
              margin: EdgeInsets.only(
                  right: 20 * ws, left: 20 * ws, bottom: 15 * ws),
              child: Center(
                child: Text(
                  errorCode == TIME_OUT
                      ? 'Phiên làm việc hết hạn.\nVui lòng đăng nhập lại'
                      : (connectionDesc != ''
                          ? connectionDesc
                          : (errorCode == ERROR_INPUT
                              ? 'Dữ liệu chưa chính xác!'
                              : (errorDesc.contains('unknowError')
                                  ? 'Có lỗi xảy ra. Vui lòng thực hiện lại'
                                  : (errorDesc.contains('serverError')
                                      ? 'Lỗi kết nối Server. Vui lòng thực hiện lại'
                                      : ' $errorDesc')))),
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                ),
              ),
            ),
            Center(
              child: RaisedGradientButton(
                marginBottom: 20 * ws,
                child: Text('OK',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 14 * fs)),
                onPressed: () async {
                  if (errorCode == TIME_OUT) {
                    ///Clear token
                    print('****Token Timeout**set token ='
                        '******');
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    prefs.setString('token', '');

                    ///Clear sqflite
                    // repositoryDatabase.databaseRepositoryStatus.clear();
                    // repositoryDatabase.databaseRepositoryVariant.clear();
                    repositoryDatabase.databaseRepositoryOffline.clear();

                    Navigator.of(context, rootNavigator: true).pop();

                    Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => MyLoginPage(url: URL_LOGOUT)),
                        (Route<dynamic> route) => false);
                  } else if (errorCode == 'login') {
                    ///login loi => chuyen ra man hinh login
                    Navigator.of(context, rootNavigator: true).pop();

                    Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => MyLoginPage(url: URL_LOGOUT)),
                        (Route<dynamic> route) => false);
                  } else {
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                },
              ),
            )
          ],
        ),
      ));
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => errorDialog);
}

Future<bool> checkConnection() async {
  bool hasConnection;

  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      hasConnection = true;
    } else {
      hasConnection = false;
    }
  } on SocketException catch (_) {
    hasConnection = false;
  }
  return hasConnection;
}

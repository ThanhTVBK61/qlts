import 'package:flutter/cupertino.dart';

class MyDialog{
  MyDialog.showDialog(
      BuildContext context, {
        String title = 'Thông báo',
        @required String msg,
        String okTitle = 'Đóng',
        String cancelTitle,
        VoidCallback okHandler,
        VoidCallback cancelHandler,
      }) {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(title),
        content: Container(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(msg),
        ),
        actions: [
          if (cancelTitle != null && cancelTitle.isNotEmpty)
            CupertinoDialogAction(
              child: Text(cancelTitle),
              isDestructiveAction: true,
              onPressed: cancelHandler ??
                      () {
                        Navigator.of(context, rootNavigator: true).pop();
                  },
            ),
          CupertinoDialogAction(
            child: Text(okTitle),
            onPressed: okHandler,
          ),
        ],
      ),
    );
  }
}
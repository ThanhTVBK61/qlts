import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qlts/constant/asset.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/widget_home/button_widget.dart';

void showSuccessDialog({BuildContext context, String message}) {
  Dialog errorDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: Container(
        padding: EdgeInsets.only(top: 20 * ws),
        width: 335,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(IMAGE_SUCCESS),
            Container(
              margin: const EdgeInsets.only(right: 50, left: 50, top: 40),
              child: message == null
                  ? Text(
                      'Thành công !',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                    )
                  : Text(
                      message,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                    ),
            ),
            Center(
              child: RaisedGradientButton(
                marginBottom: 20 * ws,
                marginTop: 20 * ws,
                child: Text('OK',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 14 * fs)),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                },
              ),
            )
          ],
        ),
      ));
  showDialog(context: context, builder: (BuildContext context) => errorDialog);
}

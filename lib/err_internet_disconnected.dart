import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:qlts/constant/asset.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/widget_dialog/dialog_error.dart';
import 'package:qlts/widget_dialog/dialog_loader.dart';
import 'package:qlts/widget_home/button_widget.dart';
import 'package:qlts/widget_login/my_login_page.dart';

class ErrInternetDisconnected extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ErrInternetDisconnectedState();
}

class ErrInternetDisconnectedState extends State<ErrInternetDisconnected> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(IMAGE_BACKGROUND,
                height: 279 * ws,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill),
            Card(
              margin: EdgeInsets.only(
                  left: 30 * ws, right: 30 * ws, bottom: 70 * ws),
              shadowColor: Colors.grey[50],
              elevation: 10.0 * ws,
              color: Colors.white.withOpacity(0.9),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Container(
                padding: EdgeInsets.only(top: 25 * ws, bottom: 25 * ws),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 20 * ws),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.warning_rounded,
                            color: Colors.red,
                            size: 35 * ws,
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 10 * ws),
                              child: Text('Lỗi kết nối internet',
                                  style: TextStyle(fontSize: 18 * fs))),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SpinKitWave(itemCount: 5, size: 20, color: Colors.blue),
                        Container(
                          margin: EdgeInsets.only(left: 20 * ws),
                          child: Text(
                            'Connecting ...',
                            style: TextStyle(fontSize: 15 * fs),
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40 * ws),
                      child: RaisedGradientButton(
                          onPressed: () async {
                            showLoaderDialog(context);
                            bool connection = await checkConnection();
                            Navigator.of(context, rootNavigator: true).pop();
                            connection
                                ? Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            MyLoginPage(url: URL_LOGOUT)),
                                    (Route<dynamic> route) => false)
                                : showErrorDialog(
                                    context: context,
                                    errorDesc: 'Vui lòng kết nối lại!');
                          },
                          child: Text(
                            'Thử lại',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 14 * fs),
                          )),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<bool> checkConnection() async {
  bool hasConnection;

  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      hasConnection = true;
    } else {
      hasConnection = false;
    }
  } on SocketException catch (_) {
    hasConnection = false;
  }
  return hasConnection;
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qlts/constant/asset.dart';
import 'package:qlts/constant/constant.dart';

class SearchBG extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SearchBGState();
}

class SearchBGState extends State<SearchBG> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset(IMAGE_BACKGROUND,
              height: 280 * ws,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.fill)
        ],
      ),
    );
  }
}

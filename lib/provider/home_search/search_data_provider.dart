import 'package:flutter/cupertino.dart';
import 'package:qlts/data_sources/data_source_search/item_search_model.dart';

class SearchDataModel extends ChangeNotifier {
  var listSearchResults = [];

  updateListSearch(List<Result> results) {
    listSearchResults = results;
    notifyListeners();
  }

  clear() {
    listSearchResults.clear();
    notifyListeners();
  }

  bool isEmpty() {
    if (listSearchResults.length > 0) return false;
    return true;
  }
}

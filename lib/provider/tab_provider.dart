import 'package:flutter/cupertino.dart';

class TabProvider extends ChangeNotifier {
  int currentTab = 0;
  bool isSearch = false;

  updateIndex(int value) {
    currentTab = value;
    notifyListeners();
  }

  updateIsSearch(bool value) {
    isSearch = value;
    notifyListeners();
  }

  clear() {
    currentTab = 0;
    isSearch = false;
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';

class HomeDataInputProvider extends ChangeNotifier {
  String soThe = '';
  String id = '';
  String serial = '';
  String qrcode = '';

  var soTheController = TextEditingController();
  var idController = TextEditingController();
  var serialController = TextEditingController();
  var qrcodeController = TextEditingController();

  ///type 3TH: -1: empty, 1: serial, 2:sothe&maso , 3: qrcode
  int type = -1;
  bool error = false;

  updateSoThe(String newSoThe) {
    soThe = newSoThe;
    notifyListeners();
  }

  updateId(String newid) {
    id = newid;
    notifyListeners();
  }

  updateSerial(String newSerial) {
    serial = newSerial;
    notifyListeners();
  }

  updateQrCode(String newQrCode) {
    qrcode = newQrCode;
    notifyListeners();
  }

  updateError(bool newError) {
    error = newError;
    notifyListeners();
  }

  updateType(int newType) {
    type = newType;
    notifyListeners();
  }

  bool empty() {
    if (id != '' || soThe != '' || serial != '' || qrcode != '') return false;
    return true;
  }

  bool validate() {
    switch (type) {
      case 1:
        if (soThe != '' && id != '' && serial == '' && qrcode == '')
          return true;
        break;
      case 2:
        if (soThe == '' && id == '' && serial != '' && qrcode == '')
          return true;
        break;
      case 3:
        if (soThe == '' && id == '' && serial == '' && qrcode != '')
          return true;
        break;
    }
    return false;
  }

  clear() {
    soThe = '';
    id = '';
    serial = '';
    qrcode = '';
    type = -1;

    soTheController.clear();
    idController.clear();
    serialController.clear();
    qrcodeController.clear();

    error = false;
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';

class QRCodeProvider extends ChangeNotifier {
  String data = '';
  bool isPause = false;

  updateData(String value) {
    data = value ?? '';

    notifyListeners();
  }

  updatePause(bool value) {
    if (isPause != value) {
      isPause = value;
      notifyListeners();
    }
  }

  isEmpty() {
    if (data == '' && isPause == false) return true;
    return false;
  }

  clear() {
    data = '';
    isPause = false;

    notifyListeners();
  }
}

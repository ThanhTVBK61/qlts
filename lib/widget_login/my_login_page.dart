import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qlts/constant/constant.dart';
import 'package:qlts/err_internet_disconnected.dart';
import 'package:qlts/widget_splash/splash_login_page.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MyLoginPage extends StatefulWidget {
  final String url;

  MyLoginPage({this.url});

  @override
  State<StatefulWidget> createState() => MyLoginPageState();
}

class MyLoginPageState extends State<MyLoginPage> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  WebViewController _controller;
  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      WebView(
        initialUrl: widget.url,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController controller) {
          Center(child: CupertinoActivityIndicator(radius: 10));
          _controller = controller;
        },
        onWebResourceError: (WebResourceError webViewErr) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          Navigator.push(
              context,
              // RouteTransition(widget: ErrInternetDisconnected())
              MaterialPageRoute(
                  builder: (context) => ErrInternetDisconnected()));
        },
        gestureNavigationEnabled: true,
        onPageFinished: (_) {
          setState(() {
            isLoading = false;
          });
          readJS();
        },
      ),
      isLoading
          ? Center(child: CupertinoActivityIndicator(radius: 10))
          : Stack()
    ]));
  }

  void readJS() async {
    String html = await _controller.evaluateJavascript(
        "window.document.getElementsByTagName('body')[0].outerHTML;");
    // print('----------HTML------------\n$html\n--------------');
    if (html.contains('"ErrorCode')) {
      String data = html
          .substring(html.indexOf('{'), html.indexOf('}') + 1)
          .replaceAll("\\", "");
      print('****************\n\n\n$data\n\n\n******************');
      if (data.contains('{"ErrorCode": "200"')) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => SplashLoginPage(data: data)));
      } else {
        Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => MyLoginPage(url: URL_LOGOUT)),
            (Route<dynamic> route) => false);
      }
    }
  }
}

import 'package:qlts/data_sources/data_sqflite/database_provider.dart';
import 'package:qlts/data_sources/data_sqflite/repository/database_repository_offline.dart';

class RepositoryDatabase {
  DatabaseRepositoryOffline databaseRepositoryOffline;

  RepositoryDatabase(DatabaseProvider databaseProvider) {

    databaseRepositoryOffline =
        DatabaseRepositoryOffline(databaseProvider: databaseProvider);
  }
}

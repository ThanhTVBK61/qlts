import 'package:flutter/cupertino.dart';

class RouteTransition extends PageRouteBuilder {
  final Widget widget;

  RouteTransition({this.widget})
      : super(
          transitionDuration: Duration(milliseconds: 600),
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            // return ScaleTransition(
            //   alignment: Alignment.bottomCenter,
            //   scale: animation,
            //   child: child,);
            var begin = Offset(1.0, 0.0);
            var end = Offset(0.0, 0.0);

            var curve = Curves.easeInCubic;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
            var offsetAnimation = animation.drive(tween);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return widget;
          },
        );
}
